## Агент 1:

Оповещение студентов об отсутствии преподавателя. Преподаватель отсылает системе сообщение о его опоздании или неявки. Система подхватывает это сообщение и запускает соотвутствующего агента. Агент в свою очередь ищет пару, связанную с ней группу, и студентов с этой группы/подгруппы. И все этим студентам отсылает сообщение о неявке или опоздании преподавателя

## Агент 2:

Оповещение преподавателя об отсутствии студента. Студент отсылает системе сообщение об отсутствии на паре. Система подхватывает это сообщение и запускает агента. Агент ищет пару, затем преподавателя связанного с парой, а также других студентов. И оповещает их об отсутствии студента

## Агент 3:

Агент по добавлению файла к предметам. Преподаватель прикрепляет файл к предмету. Агент подхватывает этот файл и всем студентам, которые связаны с этой парой отсылает уведомление о добавление файла

## Агент 4:

Агент логирования при добавлении/изменении/удалении сущностей из системы. При выполнения операций над сущностями триггерится агент и логирует все действия (кто, что, над чем)

## Агент 5: OK

Создания самих сущностей. Например при добавлении студента. Выполняется не в лоб, а запускается соответствующий агент, который в свою очередь может разбудить других. Например агента, который добавит его в группу, создаст ему зачетку и тд... ( Каждый агент представляет из себя, что-то типо информационного эксперта )

## Агент 6:

Агент по добавлению расписания студента в google календарь( на стадии продумывания). Если студент предоставит доступ к своему календарю, то агент добавит расписания студента в его календарь

## Агент 7:

Агент оповещения об изменении расписания. Студенты, расписание которых было изменено, будут оповещены о них
