import { DynamicModule, Provider } from '@nestjs/common';
import {
  Neo4jOptions,
  Neo4jModuleAsyncOptions,
  Neo4jModuleOptionsFactory,
} from './interfaces';
import { Neo4jConnection } from './neo4j.service';
import { createDriver } from './utils';
import { NEO4J_DRIVER, NEO4J_OPTIONS, NEO4J_CONNECTION } from './tokens';

export class Neo4jModule {
  private static module: DynamicModule;

  static forRoot(config: Partial<Neo4jOptions>): DynamicModule {
    Neo4jModule.module = {
      module: Neo4jModule,
      controllers: [],
      exports: [Neo4jConnection],
      providers: [
        {
          provide: NEO4J_OPTIONS,
          useValue: config,
        },
        {
          provide: NEO4J_DRIVER,
          inject: [NEO4J_OPTIONS],
          useFactory: async (config: Neo4jOptions) => createDriver(config),
        },
        {
          inject: [NEO4J_DRIVER, NEO4J_OPTIONS] as any as never,
          provide: NEO4J_CONNECTION,
          useClass: Neo4jConnection,
        },
      ],
    };

    return Neo4jModule.module;
  }

  static forRootAsync(config: Neo4jModuleAsyncOptions): DynamicModule {
    const { imports, useFactory, useClass, useExisting, inject } = config;

    const providers: Array<Provider> = [Neo4jConnection];

    if (useFactory) {
      providers.push({
        inject: inject || [],
        provide: NEO4J_OPTIONS,
        useFactory,
      });
    } else if (useClass || useExisting) {
      providers.push({
        inject: [useClass || useExisting],
        provide: NEO4J_OPTIONS,
        useFactory: (factory: Neo4jModuleOptionsFactory) =>
          factory.useNeo4jOptionsFactory(),
      });

      if (useClass) {
        providers.push({
          provide: useClass,
          useClass: useClass,
        });
      }
    }

    providers.push({
      inject: [NEO4J_OPTIONS],
      provide: NEO4J_DRIVER,
      useFactory: (options: Neo4jOptions) => createDriver(options),
    });

    providers.push({
      inject: [NEO4J_DRIVER, NEO4J_OPTIONS] as any as never,
      provide: NEO4J_CONNECTION,
      useClass: Neo4jConnection,
    });

    Neo4jModule.module = {
      module: Neo4jModule,
      imports: imports || [],
      exports: [{ provide: NEO4J_CONNECTION, useClass: Neo4jConnection }],
      providers,
    };

    return Neo4jModule.module;
  }

  static forFeature(): DynamicModule {
    return Neo4jModule.module;
  }
}
