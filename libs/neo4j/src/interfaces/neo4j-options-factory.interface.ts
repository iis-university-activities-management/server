import { Neo4jOptions } from './neo4j-config.interface';

export interface Neo4jModuleOptionsFactory {
  useNeo4jOptionsFactory: () => Neo4jOptions;
}
