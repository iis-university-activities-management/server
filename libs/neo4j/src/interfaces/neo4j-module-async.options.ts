import { InjectionToken } from '@nestjs/common';
import { Type } from '@nestjs/common';

import { Neo4jOptions } from './neo4j-config.interface';
import { Neo4jModuleOptionsFactory } from './neo4j-options-factory.interface';

export interface Neo4jModuleAsyncOptions {
  imports?: Array<any>;
  useFactory?: (...args: any[]) => Neo4jOptions;
  useClass?: Type<Neo4jModuleOptionsFactory>;
  useExisting?: Type<Neo4jModuleOptionsFactory>;
  inject?: Array<InjectionToken>;
}
