/* eslint-disable @typescript-eslint/ban-types */

import 'reflect-metadata';
import { NODE_METADATA, PROPERTY_METADATA } from '../tokens';
import { LabelMetadata, PropertyMetadata } from '../types';

export const Property = (
  options?: Pick<PropertyMetadata, 'name'>,
): PropertyDecorator => {
  return (target: Object, propertyKey: string | symbol) => {
    let classMetadata: LabelMetadata = Reflect.getMetadata(
      NODE_METADATA,
      target.constructor,
    );

    if (!classMetadata) {
      classMetadata = {
        type: '' as any,
        name: '',
        fields: {},
      };
      Reflect.defineMetadata(NODE_METADATA, classMetadata, target.constructor);
    }

    classMetadata.fields[options?.name || propertyKey] = propertyKey;
    classMetadata.fields[propertyKey] = options?.name || propertyKey;

    Reflect.defineMetadata(
      PROPERTY_METADATA,
      { name: options?.name || propertyKey },
      target,
      propertyKey,
    );
  };
};
