/* eslint-disable @typescript-eslint/ban-types */

import 'reflect-metadata';

import { TypeNode } from '../../../core';
import { RELATION_METADATA, NODE_METADATA } from '../tokens';
import { RelationMetadata } from '../types';

export const Relation = (
  options?: Pick<RelationMetadata, 'name'>,
): ClassDecorator => {
  return (constructor: Function) => {
    const { fields }: RelationMetadata = Reflect.getMetadata(
      NODE_METADATA,
      constructor,
    );
    Reflect.deleteMetadata(NODE_METADATA, constructor);

    Reflect.defineMetadata(
      RELATION_METADATA,
      {
        name: options?.name || constructor.name,
        type: TypeNode.label,
        fields: fields || {},
      } as RelationMetadata,
      constructor,
    );
  };
};
