import { NODE_METADATA, PROPERTY_METADATA } from '../tokens';
import { LabelMetadata, PropertyMetadata } from '../types';

/* eslint-disable @typescript-eslint/ban-types */
export const PrimaryGeneratedColumn = (): PropertyDecorator => {
  return (target: Object, propertyKey: string | symbol) => {
    let classMetadata: LabelMetadata = Reflect.getMetadata(
      NODE_METADATA,
      target.constructor,
    );

    if (!classMetadata) {
      classMetadata = {
        type: '' as any,
        name: '',
        fields: {},
      };
      Reflect.defineMetadata(NODE_METADATA, classMetadata, target.constructor);
    }

    classMetadata.fields['elementId'] = propertyKey;

    Reflect.defineMetadata(
      PROPERTY_METADATA,
      { name: 'elementId' } as PropertyMetadata,
      target,
      propertyKey,
    );
  };
};
