/* eslint-disable @typescript-eslint/ban-types */

import 'reflect-metadata';

import { TypeNode } from '../../../core';
import { LABEL_METADATA, NODE_METADATA } from '../tokens';
import { LabelMetadata } from '../types';

export const Label = (
  options?: Pick<LabelMetadata, 'name'>,
): ClassDecorator => {
  return (constructor: Function) => {
    const { fields }: LabelMetadata = Reflect.getMetadata(
      NODE_METADATA,
      constructor,
    );
    Reflect.deleteMetadata(NODE_METADATA, constructor);

    Reflect.defineMetadata(
      LABEL_METADATA,
      {
        name: options?.name || constructor.name,
        type: TypeNode.label,
        fields: fields || {},
      } as LabelMetadata,
      constructor,
    );
  };
};
