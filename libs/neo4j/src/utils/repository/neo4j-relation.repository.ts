import { QueryResult, Integer } from 'neo4j-driver';
import { Neo4jConnection } from '../../neo4j.service';
import { LABEL_METADATA, PROPERTY_METADATA, RELATION_METADATA } from './tokens';
import { LabelMetadata, PropertyMetadata } from './types';
import { GetPropertiesAndMetadata } from './types/neo4j-repository.type';

export class Neo4jRelationRepository<TypeRelation, FromNode, ToNode> {
  constructor(
    private readonly targetClass: new (...args: any[]) => TypeRelation,
    private readonly labelFrom: new (...args: any[]) => FromNode,
    private readonly labelTo: new (...args: any[]) => ToNode,
    private readonly neo4jConnection: Neo4jConnection,
  ) {}

  async saveByIds(
    entityFromId: number,
    relationEntity: Partial<TypeRelation>,
    entityToId: number,
  ) {
    const from = this.getClassMetadata(this.labelFrom);
    const relation = this.convertEntityToInput(
      relationEntity,
      this.targetClass,
    );
    const to = this.getClassMetadata(this.labelTo);

    const query = `
      MATCH
        (a:${from.name}),
        (b:${to.name})
      WHERE id(a)=$entityFromId AND id(b)=$entityToId
      CREATE (a)-[r:${relation.name} $relationProperties]->(b) RETURN r
    `;

    const params: any = {
      relationProperties: relation.properties,
      entityFromId: Number(entityFromId),
      entityToId: Number(entityToId),
    };

    const result = await this.neo4jConnection.write(query, params, 'neo4j');

    return this.convertResultToEntity(result.records[0], this.targetClass, 'r');
  }

  async getRightLabel(leftLabelId: number): Promise<ToNode> {
    const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    const classRelationMetadata = this.getClassMetadata(this.targetClass);
    const classLabelToMetadata = this.getClassMetadata(this.labelTo);

    const query = `MATCH (n:${classLabelFromMetadata.name})-[:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(n)=$leftLabelId RETURN s;`;
    const params = { leftLabelId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(
      result.records[0],
      this.labelTo,
      's',
    ) as ToNode;
  }

  async getRightLabelBy(leftLabel: Partial<FromNode>): Promise<ToNode> {
    // MATCH (n:Student), (s:AuthCredentials) WHERE (n)-[:AuthCredentialsRelation]->(s) AND s.login="andrei" RETURN s

    // const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    // const classRelationMetadata = this.getClassMetadata(this.targetClass);
    // const classLabelToMetadata = this.getClassMetadata(this.labelTo);
    // const query = `MATCH (n:${classLabelFromMetadata.name})-[:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(n)=$leftLabelId RETURN s;`;
    // const params = { leftLabelId };
    // const result = await this.neo4jConnection.read(query, params, 'neo4j');
    // return this.convertResultToEntity(result, this.labelTo, 's') as ToNode;

    return {} as any;
  }

  async getRightsLabel(leftLabelId: number): Promise<Array<ToNode>> {
    const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    const classRelationMetadata = this.getClassMetadata(this.targetClass);
    const classLabelToMetadata = this.getClassMetadata(this.labelTo);

    const query = `MATCH (n:${classLabelFromMetadata.name})-[:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(n)=$leftLabelId RETURN s;`;
    const params = { leftLabelId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return result.records.map(
      (rec) => this.convertResultToEntity(rec, this.labelTo, 's') as ToNode,
    );
  }

  async getLeftsLabel(rightLabelId: number): Promise<Array<FromNode>> {
    const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    const classRelationMetadata = this.getClassMetadata(this.targetClass);
    const classLabelToMetadata = this.getClassMetadata(this.labelTo);

    const query = `MATCH (n:${classLabelFromMetadata.name})-[:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(s)=$rightLabelId RETURN n;`;
    const params = { rightLabelId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return result.records.map(
      (rec) => this.convertResultToEntity(rec, this.labelFrom, 'n') as FromNode,
    );
  }

  async getLeftLabel(rightLabelId: number): Promise<FromNode> {
    const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    const classRelationMetadata = this.getClassMetadata(this.targetClass);
    const classLabelToMetadata = this.getClassMetadata(this.labelTo);

    const query = `MATCH (n:${classLabelFromMetadata.name})-[:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(s)=$rightLabelId RETURN n;`;
    const params = { rightLabelId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(
      result.records[0],
      this.labelFrom,
      'n',
    ) as FromNode;
  }

  async getRelationByIds(leftId: number, rightId: number) {
    const classLabelFromMetadata = this.getClassMetadata(this.labelFrom);
    const classRelationMetadata = this.getClassMetadata(this.targetClass);
    const classLabelToMetadata = this.getClassMetadata(this.labelTo);

    const query = `MATCH (n:${classLabelFromMetadata.name})-[r:${classRelationMetadata.name}]->(s:${classLabelToMetadata.name}) WHERE id(n)=$leftLabelId AND id(s)=$rightLabelId RETURN r;`;
    const params = { leftLabelId: leftId, rightLabelId: rightId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(
      result.records[0],
      this.targetClass,
      'r',
    ) as TypeRelation;
  }

  private convertResultToEntity(
    record: any,
    targetClass: new (...args: any[]) => any,
    letter: string,
  ): Partial<TypeRelation> | null {
    if (!record) {
      return null;
    }

    const { properties, elementId } = record.get(letter);
    const { fields }: LabelMetadata = this.getClassMetadata(targetClass);
    properties['elementId'] = Number(elementId);

    const entity = {};

    for (const key of Object.keys(properties)) {
      if (properties[key] instanceof Integer) {
        if (fields[key]) {
          entity[fields[key]] = properties[key].low;
        }
      } else if (fields[key]) {
        entity[fields[key]] = properties[key];
      }
    }

    return entity;
  }

  private convertEntityToInput(
    entity: any,
    targetClass: new (...args: any[]) => any,
  ): {
    name: string;
    properties: any;
  } {
    // delete entity.id;

    const propertiesMetadata = this.getPropertiesAndMetadata(
      entity,
      targetClass,
    );

    const classMetadata = this.getClassMetadata(targetClass);

    const whiteList = {};

    for (const key of Object.keys(entity)) {
      if (propertiesMetadata[key]) {
        const { name } = propertiesMetadata[key];
        whiteList[name] = entity[key];
      }
    }

    return {
      name: classMetadata.name,
      properties: whiteList,
    };
  }

  private getClassMetadata(targetClass: new (...args: any[]) => any) {
    const classMetadata: LabelMetadata =
      Reflect.getMetadata(LABEL_METADATA, targetClass) ||
      Reflect.getMetadata(RELATION_METADATA, targetClass);

    return classMetadata;
  }

  private getPropertiesAndMetadata(
    obj: any,
    targetClass: new (...args: any[]) => any,
  ): GetPropertiesAndMetadata {
    const propertiesMetadata: GetPropertiesAndMetadata = {};

    for (const key of Object.keys(obj)) {
      const propertyMetadata: PropertyMetadata = Reflect.getMetadata(
        PROPERTY_METADATA,
        new targetClass(),
        key,
      );
      propertiesMetadata[key] = propertyMetadata;
    }

    return propertiesMetadata;
  }
}

/*

{
  records: [
    Record {
      keys: [ 'r' ],
      length: 1,
      _fields: [
        Relationship {
          identity: Integer { low: 10, high: 0 },
          start: Integer { low: 37, high: 0 },
          end: Integer { low: 38, high: 0 },
          type: 'StudentProfile',
          properties: {
            addedAt: '2022-10-29T15:04:50.803Z',
            updatedAt: '2022-10-29T15:04:50.803Z'
          },
          elementId: '10',
          startNodeElementId: '37',
          endNodeElementId: '38'
        }
      ],
      _fieldLookup: { r: 0 }
    }
  ],
  summary: ResultSummary {
    query: {
      text: '\n' +
        '      MATCH\n' +
        '        (a:Student),\n' +
        '        (b:File)\n' +
        '      WHERE id(a)=$entityFromId AND id(b)=$entityToId\n' +
        '      CREATE (a)-[r:StudentProfile $relationProperties]->(b) RETURN r\n' +
        '    ',
      parameters: {
        relationProperties: {
          addedAt: '2022-10-29T15:04:50.803Z',
          updatedAt: '2022-10-29T15:04:50.803Z'
        },
        entityFromId: 37,
        entityToId: 38
      }
    },
    queryType: 'rw',
    counters: QueryStatistics {
      _stats: {
        nodesCreated: 0,
        nodesDeleted: 0,
        relationshipsCreated: 1,
        relationshipsDeleted: 0,
        propertiesSet: 2,
        labelsAdded: 0,
        labelsRemoved: 0,
        indexesAdded: 0,
        indexesRemoved: 0,
        constraintsAdded: 0,
        constraintsRemoved: 0
      },
      _systemUpdates: 0,
      _containsUpdates: true
    },
    updateStatistics: QueryStatistics {
      _stats: {
        nodesCreated: 0,
        nodesDeleted: 0,
        relationshipsCreated: 1,
        relationshipsDeleted: 0,
        propertiesSet: 2,
        labelsAdded: 0,
        labelsRemoved: 0,
        indexesAdded: 0,
        indexesRemoved: 0,
        constraintsAdded: 0,
        constraintsRemoved: 0
      },
      _systemUpdates: 0,
      _containsUpdates: true
    },
    plan: false,
    profile: false,
    notifications: [
      Notification {
        code: 'Neo.ClientNotification.Statement.CartesianProductWarning',
        title: 'This query builds a cartesian product between disconnected patterns.',
        description: 'If a part of a query contains multiple disconnected patterns, this will build a cartesian product between all those parts. This may produce a large amount of data and slow down query processing. While occasionally intended, it may often be possible to reformulate the query that avoids the use of this cross product, perhaps by adding a relationship between the different parts or by using OPTIONAL MATCH (identifier is: (b))',
        severity: 'WARNING',
        position: { offset: 7, line: 2, column: 1 }
      }
    ],
    server: ServerInfo {
      address: 'localhost:7687',
      agent: 'Neo4j/4.4.11',
      protocolVersion: 4.4
    },
    resultConsumedAfter: Integer { low: 0, high: 0 },
    resultAvailableAfter: Integer { low: 1, high: 0 },
    database: { name: 'neo4j' }
  }
}
*/
