import { QueryResult } from 'neo4j-driver';
import { Neo4jConnection } from '../../neo4j.service';
import { LABEL_METADATA, PROPERTY_METADATA } from './tokens';
import { LabelMetadata, PropertyMetadata } from './types';
import { GetPropertiesAndMetadata } from './types/neo4j-repository.type';

export class Neo4jLabelRepository<T> {
  constructor(
    private readonly targetClass: new (...args: any[]) => T,
    public readonly neo4jConnection: Neo4jConnection,
  ) {}

  async save(entity: Partial<Omit<T, 'id'>>): Promise<T | undefined> {
    const { name, properties } = this.convertEntityToInput(entity);

    const result = await this.neo4jConnection.write(
      `CREATE (n: ${name} $properties) RETURN n`,
      { properties },
      'neo4j',
    );

    return this.convertResultToEntity(result);
  }

  async getById(elementId: number): Promise<T> {
    const classMetadata = this.getClassMetadata();

    const query = `MATCH (n:${classMetadata.name}) WHERE id(n)=$elementId  RETURN n`;
    const params = { elementId };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(result) as any;
  }

  convertResultToEntity(result: QueryResult): any {
    if (result.records.length === 0) {
      return null;
    }

    const [record] = result.records;
    const { properties, elementId } = record.get('n');
    const { fields }: LabelMetadata = this.getClassMetadata();
    properties['elementId'] = Number(elementId);

    const entity = {};

    for (const key of Object.keys(properties)) {
      if (fields[key]) {
        entity[fields[key]] = properties[key];
      }
    }

    return entity;
  }

  convertEntityToInput(entity: any): {
    name: string;
    properties: any;
  } {
    delete entity.id;

    const propertiesMetadata = this.getPropertiesAndMetadata(entity);
    const classMetadata = this.getClassMetadata();

    const whiteList = {};

    for (const key of Object.keys(entity)) {
      if (propertiesMetadata[key]) {
        const { name } = propertiesMetadata[key];
        whiteList[name] = entity[key];
      }
    }

    return {
      name: classMetadata.name,
      properties: whiteList,
    };
  }

  /*
{
  records: [
    Record {
      keys: [ 'n' ],
      length: 1,
      _fields: [
        Node {
          identity: Integer { low: 118, high: 0 },
          labels: [ 'Student' ],
          properties: { first_name: 'Veronika', age: 20 },
          elementId: '118'
        }
      ],
      _fieldLookup: { n: 0 }
    }
  ],
  summary: ResultSummary {
    query: {
      text: 'CREATE (n: Student $properties) RETURN n',
      parameters: { properties: { first_name: 'Veronika', age: 20 } }
    },
    queryType: 'rw',
    counters: QueryStatistics {
      _stats: {
        nodesCreated: 1,
        nodesDeleted: 0,
        relationshipsCreated: 0,
        relationshipsDeleted: 0,
        propertiesSet: 2,
        labelsAdded: 1,
        labelsRemoved: 0,
        indexesAdded: 0,
        indexesRemoved: 0,
        constraintsAdded: 0,
        constraintsRemoved: 0
      },
      _systemUpdates: 0,
      _containsUpdates: true
    },
    updateStatistics: QueryStatistics {
      _stats: {
        nodesCreated: 1,
        nodesDeleted: 0,
        relationshipsCreated: 0,
        relationshipsDeleted: 0,
        propertiesSet: 2,
        labelsAdded: 1,
        labelsRemoved: 0,
        indexesAdded: 0,
        indexesRemoved: 0,
        constraintsAdded: 0,
        constraintsRemoved: 0
      },
      _systemUpdates: 0,
      _containsUpdates: true
    },
    plan: false,
    profile: false,
    notifications: [],
    server: ServerInfo {
      address: 'localhost:7687',
      agent: 'Neo4j/4.4.11',
      protocolVersion: 4.4
    },
    resultConsumedAfter: Integer { low: 1, high: 0 },
    resultAvailableAfter: Integer { low: 3, high: 0 },
    database: { name: 'neo4j' }
  }
}
  */

  getClassMetadata() {
    const classMetadata: LabelMetadata = Reflect.getMetadata(
      LABEL_METADATA,
      this.targetClass,
    );

    return classMetadata;
  }

  private getPropertiesAndMetadata(
    obj: Partial<T> | T,
  ): GetPropertiesAndMetadata {
    const propertiesMetadata: GetPropertiesAndMetadata = {};

    for (const key of Object.keys(obj)) {
      const propertyMetadata: PropertyMetadata = Reflect.getMetadata(
        PROPERTY_METADATA,
        new this.targetClass(),
        key,
      );
      propertiesMetadata[key] = propertyMetadata;
    }

    return propertiesMetadata;
  }
}
