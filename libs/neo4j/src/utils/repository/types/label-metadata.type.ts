import { TypeNode } from '../../../core';
import { NodeMetadata } from './node-metadata.type';

export interface LabelMetadata extends NodeMetadata {
  name: string;
  type: TypeNode;
}
