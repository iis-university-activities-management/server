import { PropertyMetadata } from './property-metadata.type';

export interface GetPropertiesAndMetadata {
  [key: string]: PropertyMetadata;
}
