import { TypeNode } from '../../../core';
import { NodeMetadata } from './node-metadata.type';

export interface RelationMetadata extends NodeMetadata {
  name: string;
  type: TypeNode;
}
