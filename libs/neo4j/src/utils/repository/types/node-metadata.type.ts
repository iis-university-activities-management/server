export interface NodeMetadata {
  fields: {
    [key: string | symbol]: string | symbol;
  };
}
