import neo4j from 'neo4j-driver';
import { Neo4jOptions } from '../../interfaces';

export const createDriver = async (config: Neo4jOptions) => {
  const driver = neo4j.driver(
    `${config.scheme}://${config.host}:${config.port}`,
    neo4j.auth.basic(config.username, config.password),
  );

  return driver;
};
