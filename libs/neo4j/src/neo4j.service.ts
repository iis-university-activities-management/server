import { Inject, Injectable } from '@nestjs/common';
import { Result, Session, session } from 'neo4j-driver';
import { Neo4jOptions } from './interfaces';
import { NEO4J_DRIVER, NEO4J_OPTIONS } from './tokens';

@Injectable()
export class Neo4jConnection {
  private readonly database: string;

  constructor(
    @Inject(NEO4J_DRIVER) private readonly driver,
    @Inject(NEO4J_OPTIONS) config: Neo4jOptions,
  ) {
    this.database = config.database;
  }

  read(cypher: string, params: Record<string, any>, database?: string): Result {
    const session = this.getReadSession(database);
    return session.run(cypher, params);
  }

  write(
    cypher: string,
    params: Record<string, any>,
    database?: string,
  ): Result {
    const session = this.getWriteSession(database);
    return session.run(cypher, params);
  }

  getReadSession(database?: string): Session {
    return this.driver.session({
      database: database || this.database,
      defaultAccessMode: session.READ,
    });
  }

  getWriteSession(database?: string): Session {
    return this.driver.session({
      database: database || this.database,
      defaultAccessMode: session.WRITE,
    });
  }
}
