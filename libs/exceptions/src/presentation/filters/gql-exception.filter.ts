import { ArgumentsHost, Catch, Logger } from '@nestjs/common';
import { GqlArgumentsHost } from '@nestjs/graphql';
import { Response } from 'express';

import { BaseException } from '../../core';

@Catch(BaseException)
export class GqlExceptionFilter {
  private readonly logger = new Logger('GqlExceptionFilter');

  public catch(exception: BaseException, host: ArgumentsHost) {
    console.log('some');
    const status = exception.getStatus();
    const context = exception.getContext();
    const serializedName = exception.getSerializedName();
    const description = exception.getDescription();

    this.logger.error(`context: ${context} - description: ${description}`);

    // const gqlHost = GqlArgumentsHost.create(host);

    return {
      error: {
        statusCode: status,
        [serializedName]: {
          message: description,
        },
      },
    };
  }
}
