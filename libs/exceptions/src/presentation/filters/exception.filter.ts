import { ArgumentsHost, Catch, Logger } from '@nestjs/common';
import { Response } from 'express';

import { BaseException } from '../../core';

@Catch(BaseException)
export class ExceptionFilter {
  private readonly logger = new Logger('ExceptionFilter');

  public catch(exception: BaseException, host: ArgumentsHost) {
    const status = exception.getStatus();
    const context = exception.getContext();
    const serializedName = exception.getSerializedName();
    const description = exception.getDescription();

    this.logger.error(`context: ${context} - description: ${description}`);

    const response = host.switchToHttp().getResponse<Response>();

    response.status(status).json({
      error: {
        statusCode: status,
        [serializedName]: {
          message: description,
        },
      },
    });
  }
}
