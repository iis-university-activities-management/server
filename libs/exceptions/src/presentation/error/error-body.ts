import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ErrorBody {
  @Field(() => String, { nullable: false })
  message: string;
}
