import { Field, ObjectType } from '@nestjs/graphql';
import { ErrorBody } from './error-body';

@ObjectType()
export class BaseError {
  @Field(() => Number, { nullable: false })
  statusCode: number;

  @Field(() => ErrorBody, { nullable: true })
  BadRequest?: ErrorBody;

  @Field(() => ErrorBody, { nullable: true })
  Forbidden?: ErrorBody;

  @Field(() => ErrorBody, { nullable: true })
  NotFound?: ErrorBody;

  @Field(() => ErrorBody, { nullable: true })
  Unauthorized?: ErrorBody;
}
