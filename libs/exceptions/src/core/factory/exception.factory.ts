import { BaseException, BadRequestException } from '../exceptions';

export const exceptionFactory = (): BaseException => {
  return new BadRequestException('');
};
