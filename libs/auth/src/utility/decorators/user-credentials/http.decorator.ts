import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import { TypeContext, UserCredentials } from '../../../core';

export const GetUserCredentials = createParamDecorator(
  (type: TypeContext, context: ExecutionContext): UserCredentials => {
    let request;

    switch (type) {
      case 'graphql':
        const ctx = GqlExecutionContext.create(context);
        request = ctx.getContext().req;
        break;
      case 'http':
        request = context.switchToHttp().getRequest();
        break;
    }

    return request.userCredentials;
  },
) as (type: TypeContext) => ParameterDecorator;
