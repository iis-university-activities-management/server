import { JwtPayload } from './jwt-payload.interface';

export type UserCredentials = JwtPayload;
