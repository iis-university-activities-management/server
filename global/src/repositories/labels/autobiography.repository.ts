import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Autobiography } from '../../entities';

@Injectable()
export class AutobiographyRepository extends Neo4jLabelRepository<Autobiography> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Autobiography, neo4jService);
  }
}
