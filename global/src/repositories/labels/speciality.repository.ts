import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Specialty } from '../../entities';

@Injectable()
export class SpecialityRepository extends Neo4jLabelRepository<Specialty> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Specialty, neo4jService);
  }
}
