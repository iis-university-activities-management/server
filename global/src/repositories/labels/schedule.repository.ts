import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Schedule } from '../../entities';

@Injectable()
export class ScheduleRepository extends Neo4jLabelRepository<Schedule> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Schedule, neo4jService);
  }
}
