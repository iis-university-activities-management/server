import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { AuthCredentials } from '../../entities';

@Injectable()
export class AuthCredentialsRepository extends Neo4jLabelRepository<AuthCredentials> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(AuthCredentials, neo4jService);
  }

  async getByLogin(login: string): Promise<AuthCredentials> {
    const classMetadata = this.getClassMetadata();

    const query = `MATCH (n:${classMetadata.name}) WHERE n.login=$login  RETURN n`;
    const params = { login };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(result) as any;
  }
}
