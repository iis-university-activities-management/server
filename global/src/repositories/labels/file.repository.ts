import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { File } from '../../entities';

@Injectable()
export class FileRepository extends Neo4jLabelRepository<File> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(File, neo4jService);
  }
}
