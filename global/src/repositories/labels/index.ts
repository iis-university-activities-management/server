export * from './file.repository';
export * from './autobiography.repository';
export * from './speciality.repository';
export * from './auth-credentials.repository';
export * from './schedule.repository';
