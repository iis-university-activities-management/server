import { Inject, Injectable } from '@nestjs/common';

import { Neo4jRelationRepository, Neo4jConnection } from '@uam/neo4j';

import { AuthCredentials, AuthCredentialsRelation } from '../../entities';

@Injectable()
export class AuthCredentialsRelationRepository<
  NodeFrom,
> extends Neo4jRelationRepository<
  AuthCredentialsRelation,
  NodeFrom,
  AuthCredentials
> {
  constructor(
    nodeFrom: new (...args: any[]) => NodeFrom,
    neo4jService: Neo4jConnection,
  ) {
    super(AuthCredentialsRelation, nodeFrom, AuthCredentials, neo4jService);
  }
}
