import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class Specialty {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  name: string;
}
