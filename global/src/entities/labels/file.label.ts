import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  uri: string;

  @Property()
  key: string;
}
