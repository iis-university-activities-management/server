import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class AuthCredentials {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  login: string;

  @Property()
  password: string;
}
