import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class Schedule {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  name: string;
}
