import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class Autobiography {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  text: string;
}
