import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class AuthCredentialsRelation {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  addedAt: string;

  @Property()
  updatedAt: string;
}
