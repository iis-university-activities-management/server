import { ApplicationConfig } from './application';
import { AwsS3Config } from './aws/s3/aws-s3-config.interface';
import { GraphqlConfig } from './graphql';
import { JwtConfig } from './jwt';
import { Neo4jConfig } from './neo4j';

export interface Config
  extends ApplicationConfig,
    GraphqlConfig,
    JwtConfig,
    Neo4jConfig,
    AwsS3Config {}
