import { ApplicationConfig } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfig => ({
  application: {
    port: Number(process.env.TEACHERS_PORT),
    host: process.env.TEACHERS_HOST,
  },
});
