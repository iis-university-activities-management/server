import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

import { ExceptionFilter, exceptionFactory } from '@uam/exceptions';

import { Config } from './config';
import { TeachersModule } from './teachers.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(TeachersModule);

  app.setGlobalPrefix('api');

  app.enableCors();

  const builder = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('UAM api')
    .setDescription('Some description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, builder);

  SwaggerModule.setup('api', app, document);

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory,
      forbidUnknownValues: false,
      transform: true,
    }),
  );

  app.useGlobalFilters(new ExceptionFilter());

  const configService = app.get<ConfigService<Config>>(ConfigService);
  const { port, host } = configService.get('application');

  await app.listen(port, host);

  console.log(
    `[SERVER TEACHERS STARTING] server listens to ${await app.getUrl()}`,
  );
}
bootstrap();
