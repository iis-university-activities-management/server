import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';

import { Neo4jModule } from '@uam/neo4j';
import { AwsS3Module } from '@uam/aws/s3';

import {
  configuration,
  validate,
  GraphQlConfigFactory,
  Neo4jConfigFactory,
  AwsS3ConfigFactory,
} from './config';
import { AmqpModule } from './modules/amqp';
import { InboundModule, OutboundModule } from './modules/event-bus';
import { TeacherModule } from './modules/teacher';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: configuration,
      validate,
    }),
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      useClass: GraphQlConfigFactory,
    }),
    AwsS3Module.forRootAsync({
      useClass: AwsS3ConfigFactory,
    }),
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
    AmqpModule,
    InboundModule,
    OutboundModule,
    TeacherModule,
  ],
})
export class TeachersModule {}
