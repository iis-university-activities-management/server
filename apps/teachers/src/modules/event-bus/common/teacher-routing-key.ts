export enum TeacherRoutingKey {
  teacherLeftClass = 'teacher.outbound.event.leftClassEvent',
  studentLeftClass = 'teacher.inbound.event.studentLeftClass',
}
//   studentCreatedEvent = 'student.outbound.event.studentCreated',
//   getTeacherByEmail = 'student.getstudentByEmail',
//   getTeachersByIds = 'student.inbound.rpc.getstudentsByIds',
//   getTeacherById = 'student.inbound.rpc.getstudentById',
//   getstudentsByEmails = 'student.getstudentsByEmails',
//   createActivestudent = 'student.mutation.createActivestudent',
//   createInactivestudent = 'student.mutation.createInactivestudent',
//   updatestudentAuthProvider = 'student.mutation.updateAuthProvider',
//   setstudentPassword = 'student.inbound.rpc.setstudentPassword',
//   updatestudentInfo = 'student.inbound.rpc.updatestudentInfo',
//   updatestudentEmail = 'student.inbound.rpc.updatestudentEmail',
//
