import { ConsumeMessage } from 'amqplib';
import { Exchange, TeacherQueue, TeacherRoutingKey } from '../common';

export abstract class TeacherLeftClassAgentInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: TeacherRoutingKey.teacherLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
