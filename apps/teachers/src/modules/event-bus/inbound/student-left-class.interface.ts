import { ConsumeMessage } from 'amqplib';
import { Exchange, TeacherRoutingKey } from '../common';

export abstract class StudentLeftClassAgentInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: TeacherRoutingKey.studentLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
