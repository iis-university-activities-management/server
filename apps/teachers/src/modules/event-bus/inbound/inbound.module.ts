import { Module } from '@nestjs/common';
import {
  ClassTeacherRelationRepository,
  TeacherRepository,
} from '../../teacher/infrastructure';
import { OutboundModule } from '../outbound';
import { TeacherLeftClassAgent } from './teacher-left-class.agent';
import { Neo4jModule } from '@uam/neo4j';
import { Neo4jConfigFactory } from '../../../config';
import { StudentLeftClassAgent } from './student-left-class.agent';
import { MailerModule } from '@nestjs-modules/mailer';
import { resolve } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ClassRepository } from '../../../../../subjects/src/modules/subject/infrastructure';

@Module({
  imports: [
    OutboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
          user: 'igrakkaunt@gmail.com',
          pass: 'nbcroowkjsdyewxd',
        },
      },
      defaults: {
        from: '"IIS 2.0" <iis@uam.com>',
      },
      template: {
        dir: resolve(__dirname, '..', '..', '..', 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [
    TeacherLeftClassAgent,
    ClassTeacherRelationRepository,
    TeacherRepository,
    ClassRepository,
    StudentLeftClassAgent,
  ],
})
export class InboundModule {}
