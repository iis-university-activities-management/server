import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import {
  TeacherRepository,
  ClassTeacherRelationRepository,
} from '../../teacher/infrastructure';
import { TeacherQueue } from '../common';
import { TeacherLeftClassToGroupAgentEvent } from '../outbound/events';
import { TeacherLeftClassAgentInterface } from './teacher-left-class.interface';

interface CreateStudentAgentInput {
  teacherId: number;
  classId: number;
  message: string;
}

@Injectable()
export class TeacherLeftClassAgent implements TeacherLeftClassAgentInterface {
  private readonly logger = new Logger(TeacherLeftClassAgent.name);

  constructor(
    private readonly teacherRepository: TeacherRepository,
    // private readonly classTeacherRelationRepository: ClassTeacherRelationRepository,
    private readonly teacherLeftClassToGroupAgentEvent: TeacherLeftClassToGroupAgentEvent,
  ) {}

  @RabbitSubscribe({
    ...TeacherLeftClassAgentInterface.config,
    queue: TeacherQueue.teacherQueue,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: CreateStudentAgentInput) {
    const { classId, message, teacherId } = params;

    this.logger.log('[AGENT STARTING]');

    const teacher = await this.teacherRepository.getById(teacherId);

    if (teacher) {
      console.log({ teacher });

      await this.teacherLeftClassToGroupAgentEvent.execute({
        teacher,
        classId,
        message,
      });
    }

    this.logger.log('[AGENT FINISHED]');
  }
}
