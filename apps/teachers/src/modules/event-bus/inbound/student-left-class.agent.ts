import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { IStudent } from '../../../../../students/src/modules/student/domain';
import { ClassRepository } from '../../../../../subjects/src/modules/subject/infrastructure';
import {
  TeacherRepository,
  ClassTeacherRelationRepository,
} from '../../teacher/infrastructure';
import { TeacherQueue } from '../common';
import { StudentLeftClassAgentInterface } from './student-left-class.interface';

interface StudentLeftClassAgentInput {
  student: IStudent;
  classId: number;
  message: string;
}

/*
  student: {
    lastName: 'Воропаев',
    firstName: 'Андрей',
    birthDay: '2022-12-12T00:20:29.383Z',
    middleName: 'Александрович',
    email: 'igrakkaunt@gmail.com',
    id: 43
  },
  classId: 51,
  message: ''
*/

const weeks = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

@Injectable()
export class StudentLeftClassAgent implements StudentLeftClassAgentInterface {
  private readonly logger = new Logger(StudentLeftClassAgent.name);

  constructor(
    private readonly classTeacherRelationRepository: ClassTeacherRelationRepository,
    private readonly mailerService: MailerService,
    private readonly classRepository: ClassRepository,
  ) {}

  @RabbitSubscribe({
    ...StudentLeftClassAgentInterface.config,
    queue: TeacherQueue.studentLeftClass,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: StudentLeftClassAgentInput) {
    console.log({ params });

    const { classId, message, student } = params;

    const leftClass = await this.classRepository.getById(classId);

    if (leftClass) {
      console.log({ class: leftClass });

      if (leftClass) {
        const teacher = await this.classTeacherRelationRepository.getRightLabel(
          classId,
        );

        console.log({ teacher });

        if (teacher) {
          if (teacher.email) {
            this.logger.log(`Sent message to email: ${teacher.email}`);

            await this.mailerService.sendMail({
              to: teacher.email,
              // from: '"Support Team" <support@example.com>', // override default from
              subject: 'IIS Notification',
              template: './teacher-left-event', // `.hbs` extension is appended automatically
              context: {
                // ✏️ filling curly brackets with content
                teacher: `${student.lastName} ${student.firstName} ${student.middleName}`,
                message:
                  message ||
                  `Student is late for a class <${leftClass.name}> ${
                    weeks[Number(leftClass.dayOfTheWeek)]
                  } at ${leftClass.starts}`,
              },
            });
          }
        }
      }
    }
    this.logger.log('[AGENT FINISHED]');
  }
}
