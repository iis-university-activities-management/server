import { ConsumeMessage } from 'amqplib';
import { Exchange, TeacherRoutingKey } from '../../common';

export abstract class TeacherLeftClassInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: TeacherRoutingKey.teacherLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
