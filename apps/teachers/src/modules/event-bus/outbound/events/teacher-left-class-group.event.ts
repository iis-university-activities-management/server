import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { TeacherLeftClassToGroupAgentEventInterface } from './teacher-left-class-group.interface';

@Injectable()
export class TeacherLeftClassToGroupAgentEvent
  implements TeacherLeftClassToGroupAgentEventInterface
{
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } =
      TeacherLeftClassToGroupAgentEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
