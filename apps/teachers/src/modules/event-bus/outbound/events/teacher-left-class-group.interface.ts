import { ConsumeMessage } from 'amqplib';
import { GroupRoutingKey } from '../../../../../../groups/src/modules/event-bus/common';
import { Exchange } from '../../common';

export abstract class TeacherLeftClassToGroupAgentEventInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: GroupRoutingKey.teacherLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
