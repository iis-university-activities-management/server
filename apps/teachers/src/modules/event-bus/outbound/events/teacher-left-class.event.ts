import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { TeacherLeftClassInterface } from './teacher-left-class.interface';

@Injectable()
export class TeacherLeftClassEvent implements TeacherLeftClassInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = TeacherLeftClassInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
