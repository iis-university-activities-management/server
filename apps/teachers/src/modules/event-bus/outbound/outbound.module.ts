import { Module } from '@nestjs/common';
import {
  TeacherLeftClassEvent,
  TeacherLeftClassToGroupAgentEvent,
} from './events';

@Module({
  providers: [TeacherLeftClassEvent, TeacherLeftClassToGroupAgentEvent],
  exports: [TeacherLeftClassEvent, TeacherLeftClassToGroupAgentEvent],
})
export class OutboundModule {}
