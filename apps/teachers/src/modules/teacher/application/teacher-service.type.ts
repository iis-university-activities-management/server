export interface UploadProfileParameters {
  teacherId: number;
  profile: Express.Multer.File;
}
export interface UploadProfileResult {
  uri: string;
  key: string;
}

export interface GetProfileParameters {
  teacherId: number;
}
export interface GetProfileResult {
  uri: string;
  key: string;
}

export interface GetTeacherByIdParameters {
  teacherId: number;
}

export interface GetTeacherAutobiographyParameters {
  teacherId: number;
}
export interface GetTeacherAutobiographyResult {
  text: string;
}

export interface GetTeacherSpecialtyParameters {
  teacherId: number;
}
export interface GetTeacherSpecialtyResult {
  speciality: string;
  admissionAt: string;
  course: number;
}
