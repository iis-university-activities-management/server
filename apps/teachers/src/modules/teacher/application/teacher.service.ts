import { Injectable } from '@nestjs/common';
import { AwsS3Service } from '@uam/aws/s3';
import { UnauthorizedException, BadRequestException } from '@uam/exceptions';
import { TeacherLeftClassEvent } from '../../event-bus/outbound/events';
import { AuthCredentialsRepository } from '@uam/global';

import { TeacherDomain } from '../domain';
import {
  ClassTeacherRelationRepository,
  Teacher,
  TeacherAuthCredentialsRelationRepository,
  TeacherRepository,
} from '../infrastructure';
import { GetTeacherByIdParameters } from './teacher-service.type';
import {
  GroupScheduleRelationRepository,
  ScheduleClassRelationRepository,
  Week,
} from '../../../../../groups/src/modules/group/infrastructure';
import { ClassSubgroupRelationRepository } from '../../../../../subjects/src/modules/subject/infrastructure';
import { IClass } from '../../../../../subjects/src/modules/subject/domain';
import {
  IGroup,
  ISubgroup,
} from '../../../../../groups/src/modules/group/domain';
import { ClassOnWeekRelationRepository } from '../../../../../groups/src/modules/group/infrastructure/repositories/class-on-week-ralation.repository';
import { StudentProfileRelationRepository } from '../../../../../students/src/modules/student/infrastructure';
import { TeacherProfileRelationRepository } from '../../../../../students/src/modules/student/infrastructure/repositories/teacher-profile.repository';

@Injectable()
export class TeacherService {
  constructor(
    private readonly teacherRepository: TeacherRepository,
    private readonly teacherLeftClass: TeacherLeftClassEvent,
    private readonly scheduleClassRelationRepository: ScheduleClassRelationRepository,
    private readonly groupScheduleRelationRepository: GroupScheduleRelationRepository,
    private readonly classTeacherRelationRepository: ClassTeacherRelationRepository,
    private readonly teacherAuthCredentialsRelationRepository: TeacherAuthCredentialsRelationRepository,
    private readonly classSubgroupRelationRepository: ClassSubgroupRelationRepository,
    private readonly authCredentialsRepository: AuthCredentialsRepository,
    private readonly classOnWeekRelationRepository: ClassOnWeekRelationRepository,
    private readonly teacherProfileRelationRepository: TeacherProfileRelationRepository,
  ) {}

  async getTeacherById(params: GetTeacherByIdParameters): Promise<Teacher> {
    const { teacherId } = params;

    const teacher = await this.teacherRepository.getById(teacherId);

    if (!teacher) {
      throw new BadRequestException(
        'getTeacherById',
        `teacher by id <${teacherId}> not found`,
      );
    }

    return teacher;
  }

  async sendEvent(input: {
    teacherId: number;
    classId: number;
    message?: string;
  }) {
    await this.teacherLeftClass.execute(input);
  }

  async getTeacherSchedule(params: { teacherId: number }) {
    const { teacherId } = params;

    const classes = await this.classTeacherRelationRepository.getLeftsLabel(
      teacherId,
    );

    const result: Array<{
      class: IClass;
      group: IGroup;
      subgroups: Array<ISubgroup>;
      weeks: Array<Week>;
    }> = [];

    for (const _class of classes) {
      const schedule = await this.scheduleClassRelationRepository.getLeftLabel(
        _class.id,
      );

      const weeks = await this.classOnWeekRelationRepository.getRightsLabel(
        _class.id,
      );

      const group = await this.groupScheduleRelationRepository.getLeftLabel(
        schedule.id,
      );
      const subgroups =
        await this.classSubgroupRelationRepository.getRightsLabel(_class.id);

      result.push({
        class: _class,
        group,
        subgroups,
        weeks,
      });
    }

    return {
      data: result,
    };
  }

  async login(params: { login: string; password: string }) {
    const { login, password } = params;
    const credentials = await this.authCredentialsRepository.getByLogin(login);

    if (!credentials) {
      throw new UnauthorizedException(
        `Teacher with this login not found: <${login}>`,
      );
    }

    if (password !== credentials.password) {
      throw new UnauthorizedException(`Login or password is incorrect`);
    }

    const teacher =
      await this.teacherAuthCredentialsRelationRepository.getLeftLabel(
        credentials.id,
      );

    if (!teacher) {
      throw new UnauthorizedException(`Login or password is incorrect`);
    }

    const profile = await this.teacherProfileRelationRepository.getRightLabel(
      teacher.id,
    );

    return {
      data: { ...teacher, profile },
    };
  }
}
