import { Module } from '@nestjs/common';

import { Neo4jModule } from '@uam/neo4j';
import {
  AutobiographyRepository,
  FileRepository,
  SpecialityRepository,
  AuthCredentialsRepository,
} from '@uam/global';

import { Neo4jConfigFactory } from '../../config';
import { InboundModule, OutboundModule } from '../event-bus';
import { TeacherService } from './application';
import { TeacherDomain } from './domain';
import {
  ClassTeacherRelationRepository,
  TeacherAuthCredentialsRelationRepository,
  TeacherRepository,
} from './infrastructure';
import {
  TeacherContoller,
  TeacherWsGateway,
  TeacherResolver,
  TeacherHandler,
  AuthTeacherContoller,
} from './presentation';
import {
  GroupScheduleRelationRepository,
  ScheduleClassRelationRepository,
} from '../../../../groups/src/modules/group/infrastructure';
import { ClassSubgroupRelationRepository } from '../../../../subjects/src/modules/subject/infrastructure';
import { ClassOnWeekRelationRepository } from '../../../../groups/src/modules/group/infrastructure/repositories/class-on-week-ralation.repository';
import {
  StudentProfileRelation,
  StudentProfileRelationRepository,
} from '../../../../students/src/modules/student/infrastructure';
import { TeacherProfileRelationRepository } from '../../../../students/src/modules/student/infrastructure/repositories/teacher-profile.repository';

@Module({
  imports: [
    OutboundModule,
    InboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [
    TeacherDomain,
    TeacherService,
    TeacherWsGateway,
    TeacherHandler,
    TeacherResolver,
    TeacherRepository,
    FileRepository,
    SpecialityRepository,
    ClassTeacherRelationRepository,
    ScheduleClassRelationRepository,
    GroupScheduleRelationRepository,
    ClassSubgroupRelationRepository,
    AutobiographyRepository,
    ClassOnWeekRelationRepository,
    TeacherProfileRelationRepository,
    AuthCredentialsRepository,
    TeacherAuthCredentialsRelationRepository,
  ],
  controllers: [TeacherContoller, AuthTeacherContoller],
})
export class TeacherModule {}
