import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { TeacherService } from '../application';
import { TeacherLoginInput } from './inputs';
import { TeacherInfoResult } from './results';

@ApiTags('Teachers Auth')
@Controller('v1/auth')
@UsePipes(ValidationPipe)
export class AuthTeacherContoller {
  constructor(private readonly teacherService: TeacherService) {}

  @Post('/sign-in')
  @ApiResponse({
    type: TeacherInfoResult,
  })
  async teacherInfo(
    @Body() input: TeacherLoginInput,
  ): Promise<TeacherInfoResult> {
    const { data } = await this.teacherService.login(input);

    return {
      data,
    };
  }
}
