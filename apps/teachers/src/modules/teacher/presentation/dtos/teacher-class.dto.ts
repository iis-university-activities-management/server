import { ApiProperty } from '@nestjs/swagger';
import {
  GroupDto,
  SubgroupDto,
} from '../../../../../../groups/src/modules/group/presentation/dtos';
import { ClassDto } from '../../../../../../subjects/src/modules/subject/presentation/dtos';

export class TeacherClassDto {
  @ApiProperty({ type: ClassDto, nullable: false })
  class: ClassDto;
  @ApiProperty({ type: GroupDto, nullable: false })
  group: GroupDto;
  @ApiProperty({ type: SubgroupDto, isArray: true, nullable: false })
  subgroups: Array<SubgroupDto>;
}
