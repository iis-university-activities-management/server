import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class GetTeacherScheduleInput {
  @ApiProperty({ type: Number, nullable: false })
  @IsNumber()
  @IsNotEmpty()
  teacherId: number;
}
