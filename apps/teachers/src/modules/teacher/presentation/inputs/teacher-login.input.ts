import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class TeacherLoginInput {
  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  login: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  password: string;
}
