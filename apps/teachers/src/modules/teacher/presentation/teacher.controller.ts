import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

import { TeacherService } from '../application';
import { TeacherEventInput } from './inputs';
import { GetTeacherScheduleInput } from './inputs/get-teacher-schedule.input';
import { GetTeacherScheduleResult } from './results';

@ApiTags('Teachers Info')
@Controller('v1/teachers')
@UsePipes(ValidationPipe)
export class TeacherContoller {
  constructor(private readonly teacherService: TeacherService) {}

  @Post('/event')
  @ApiBearerAuth()
  async teacherInfo(@Body() input: TeacherEventInput) {
    await this.teacherService.sendEvent({ ...input });
  }

  @Post('/schedule')
  @ApiResponse({
    type: GetTeacherScheduleResult,
  })
  @ApiBearerAuth()
  async getTeacherSchedule(
    @Body() input: GetTeacherScheduleInput,
  ): Promise<GetTeacherScheduleResult> {
    const { data } = await this.teacherService.getTeacherSchedule({ ...input });

    return {
      data,
    };
  }
}
