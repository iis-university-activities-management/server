import { WebSocketGateway } from '@nestjs/websockets';
import { TeacherService } from '../application';

@WebSocketGateway({ namespace: 'teachers' })
export class TeacherWsGateway {
  constructor(private readonly studentService: TeacherService) {}
}
