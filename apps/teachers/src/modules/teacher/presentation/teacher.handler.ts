import { Injectable } from '@nestjs/common';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class TeacherHandler {
  @RabbitSubscribe({ queue: 'test' })
  getMessage(message: any) {
    console.log('message from raggitmq', message);
  }
}
