import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { TeacherDto } from '../dtos';

export class TeacherInfoResult {
  @ApiProperty({ type: TeacherDto, nullable: true })
  data?: TeacherDto;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
