import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { TeacherClassDto } from '../dtos';

export class GetTeacherScheduleResult {
  @ApiProperty({ type: TeacherClassDto, isArray: true, nullable: true })
  data?: Array<TeacherClassDto>;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
