import { Inject, Injectable } from '@nestjs/common';

import { NEO4J_CONNECTION, Neo4jConnection } from '@uam/neo4j';
import { AuthCredentialsRelationRepository } from '@uam/global';

import { Teacher } from '../entities';

@Injectable()
export class TeacherAuthCredentialsRelationRepository extends AuthCredentialsRelationRepository<Teacher> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Teacher, neo4jService);
  }
}
