import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Teacher } from '../entities';

@Injectable()
export class TeacherRepository extends Neo4jLabelRepository<Teacher> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Teacher, neo4jService);
  }
}
