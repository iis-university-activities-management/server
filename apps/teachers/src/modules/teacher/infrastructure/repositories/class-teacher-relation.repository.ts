import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Class } from '../../../../../../subjects/src/modules/subject/infrastructure';
import { ClassTeacherRelation, Teacher } from '../entities';

@Injectable()
export class ClassTeacherRelationRepository extends Neo4jRelationRepository<
  ClassTeacherRelation,
  Class,
  Teacher
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(ClassTeacherRelation, Class, Teacher, neo4jService);
  }
}
