import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { ITeacher } from '../../domain';

@Label()
export class Teacher implements ITeacher {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  firstName: string;

  @Property()
  middleName: string;

  @Property()
  lastName: string;

  @Property()
  email: string;
}
