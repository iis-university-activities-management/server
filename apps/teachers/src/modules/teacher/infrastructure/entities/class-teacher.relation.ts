import { Label, PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';

import { IClassTeacherRelation } from '../../domain';

@Relation()
export class ClassTeacherRelation implements IClassTeacherRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
