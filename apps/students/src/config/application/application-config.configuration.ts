import { ApplicationConfig } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfig => ({
  application: {
    port: Number(process.env.STUDENTS_PORT),
    host: process.env.STUDENTS_HOST,
  },
});
