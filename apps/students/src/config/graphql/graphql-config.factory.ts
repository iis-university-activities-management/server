import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';

import { Config } from '../config.interface';

@Injectable()
export class GraphQlConfigFactory implements GqlOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createGqlOptions() {
    // const { some } = this.configService.get('graphql');
    return {
      autoSchemaFile: true,
      playground: true,
    };
  }
}
