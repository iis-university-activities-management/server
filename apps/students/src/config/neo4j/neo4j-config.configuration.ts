import { Neo4jConfig } from './neo4j-config.interface';

export const neo4jConfiguration = (): Neo4jConfig => ({
  neo4j: {
    host: process.env.NEO4J_HOST,
    password: process.env.NEO4J_PASSWORD,
    port: Number(process.env.NEO4J_PORT),
    scheme: 'bolt',
    username: process.env.NEO4J_USERNAME,
    database: process.env.NEO4J_DATABASE,
  },
});
