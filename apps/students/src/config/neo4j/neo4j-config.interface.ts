import { Neo4jOptions } from '@uam/neo4j';

export interface Neo4jConfig {
  neo4j: Neo4jOptions;
}
