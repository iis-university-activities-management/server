export enum StudentRoutingKey {
  studentCreatedEvent = 'student.outbound.event.studentCreated',
  studentCreateEvent = 'student.outbound.event.studentCreate',
  studentLeftClassEvent = 'student.outbound.event.studentLeftClassEvent',
}
//   studentCreatedEvent = 'student.outbound.event.studentCreated',
//   getStudentByEmail = 'student.getstudentByEmail',
//   getStudentsByIds = 'student.inbound.rpc.getstudentsByIds',
//   getStudentById = 'student.inbound.rpc.getstudentById',
//   getstudentsByEmails = 'student.getstudentsByEmails',
//   createActivestudent = 'student.mutation.createActivestudent',
//   createInactivestudent = 'student.mutation.createInactivestudent',
//   updatestudentAuthProvider = 'student.mutation.updateAuthProvider',
//   setstudentPassword = 'student.inbound.rpc.setstudentPassword',
//   updatestudentInfo = 'student.inbound.rpc.updatestudentInfo',
//   updatestudentEmail = 'student.inbound.rpc.updatestudentEmail',
//
