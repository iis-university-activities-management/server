import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import { StudentRepository } from '../../student/infrastructure';
import { StudentQueue } from '../common';
import { StudentCreatedEvent } from '../outbound/events/students-created.event';
import { CreateStudentAgentInterface } from './create-student.interface';

interface CreateStudentAgentInput {
  firstName: string;
  middleName: string;
  lastName: string;
  birthDay: string;
  groupName: string;
  email: string;
}

@Injectable()
export class CreateStudentAgent implements CreateStudentAgentInterface {
  private readonly logger = new Logger(CreateStudentAgent.name);

  constructor(
    private readonly studentRepository: StudentRepository,
    private readonly studentCreatedEvent: StudentCreatedEvent,
  ) {}

  @RabbitSubscribe({
    ...CreateStudentAgentInterface.config,
    queue: StudentQueue.studentQueue,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: CreateStudentAgentInput) {
    const { birthDay, firstName, lastName, middleName, groupName, email } =
      params;

    this.logger.log('[AGENT STARTING]');

    const student = await this.studentRepository.save({
      birthDay,
      firstName,
      lastName,
      middleName,
      email,
    });

    if (student) {
      console.log({ student });

      await this.studentCreatedEvent.execute({
        student,
        groupName,
      });
    }

    this.logger.log('[AGENT FINISHED]');
  }
}
