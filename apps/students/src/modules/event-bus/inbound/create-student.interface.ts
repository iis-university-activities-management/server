import { ConsumeMessage } from 'amqplib';
import { StudentRoutingKey } from '../common';
import { Exchange } from '../outbound/events/student-created-event.interface';

export abstract class CreateStudentAgentInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: StudentRoutingKey.studentCreateEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
