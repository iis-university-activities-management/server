import { ConsumeMessage } from 'amqplib';
import { StudentRoutingKey } from '../common';
import { Exchange } from '../outbound/events/student-created-event.interface';

export abstract class StudentLeftClassAgentInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: StudentRoutingKey.studentLeftClassEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
