import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import { StudentRepository } from '../../student/infrastructure';
import { StudentQueue } from '../common';
import { StudentLeftClassToTeacherAgentEvent } from '../outbound/events/student-left-class-teacher.event';
import { StudentLeftClassAgentInterface } from './student-left-class-agent.interface';

interface StudentLeftClassAgentInput {
  message?: string;
  classId: number;
  studentId: number;
}

@Injectable()
export class StudentLeftClassAgent implements StudentLeftClassAgentInterface {
  private readonly logger = new Logger(StudentLeftClassAgent.name);

  constructor(
    private readonly studentRepository: StudentRepository,
    private readonly studentLeftClassToTeacherAgentEvent: StudentLeftClassToTeacherAgentEvent,
  ) {}

  @RabbitSubscribe({
    ...StudentLeftClassAgentInterface.config,
    queue: StudentQueue.studentAgentLeftQueue,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: StudentLeftClassAgentInput) {
    const { classId, studentId, message } = params;

    this.logger.log('[AGENT STARTING]');

    const student = await this.studentRepository.getById(studentId);

    if (student) {
      console.log({ student });

      await this.studentLeftClassToTeacherAgentEvent.execute({
        student,
        classId,
        message,
      });
    }

    this.logger.log('[AGENT FINISHED]');
  }
}
