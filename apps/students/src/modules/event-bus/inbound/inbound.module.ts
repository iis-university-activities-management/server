import { Module } from '@nestjs/common';
import { StudentRepository } from '../../student/infrastructure';
import { OutboundModule } from '../outbound';
import { CreateStudentAgent } from './create-student.agent';

import { Neo4jModule } from '@uam/neo4j';
import { Neo4jConfigFactory } from '../../../config';
import { StudentLeftClassAgent } from './student-left-class.agent';

@Module({
  imports: [
    OutboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [CreateStudentAgent, StudentRepository, StudentLeftClassAgent],
})
export class InboundModule {}
