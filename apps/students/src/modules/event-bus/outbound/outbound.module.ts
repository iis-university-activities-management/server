import { Module } from '@nestjs/common';
import { StudentLeftClassToTeacherAgentEvent } from './events/student-left-class-teacher.event';
import { StudentLeftClassEvent } from './events/student-left-class.event';
import { StudentCreateEvent } from './events/students-create.event';
import { StudentCreatedEvent } from './events/students-created.event';

@Module({
  providers: [
    StudentCreatedEvent,
    StudentCreateEvent,
    StudentLeftClassEvent,
    StudentLeftClassToTeacherAgentEvent,
  ],
  exports: [
    StudentCreatedEvent,
    StudentCreateEvent,
    StudentLeftClassEvent,
    StudentLeftClassToTeacherAgentEvent,
  ],
})
export class OutboundModule {}
