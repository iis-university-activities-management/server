import { ConsumeMessage } from 'amqplib';
import { StudentRoutingKey } from '../../common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class StudentCreateEventInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: StudentRoutingKey.studentCreateEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
