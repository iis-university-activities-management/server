import { ConsumeMessage } from 'amqplib';
import { StudentRoutingKey } from '../../common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class StudentLeftClassEventInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: StudentRoutingKey.studentLeftClassEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
