import { ConsumeMessage } from 'amqplib';
import { StudentRoutingKey } from '../../common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class StudentCreatedEventInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: StudentRoutingKey.studentCreatedEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
