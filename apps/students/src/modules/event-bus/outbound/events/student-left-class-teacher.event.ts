import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { StudentLeftClassToTeacherAgentEventInterface } from './student-left-class-teacher.interface';

@Injectable()
export class StudentLeftClassToTeacherAgentEvent
  implements StudentLeftClassToTeacherAgentEventInterface
{
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } =
      StudentLeftClassToTeacherAgentEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
