import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { StudentCreateEventInterface } from './student-create-event.interface';

@Injectable()
export class StudentCreateEvent implements StudentCreateEventInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = StudentCreateEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
