import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { StudentLeftClassEventInterface } from './student-left-class-event.interface';

@Injectable()
export class StudentLeftClassEvent implements StudentLeftClassEventInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = StudentLeftClassEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
