import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { StudentCreatedEventInterface } from './student-created-event.interface';

@Injectable()
export class StudentCreatedEvent implements StudentCreatedEventInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = StudentCreatedEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
