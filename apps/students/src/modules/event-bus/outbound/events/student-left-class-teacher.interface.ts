import { ConsumeMessage } from 'amqplib';
import { TeacherRoutingKey } from '../../../../../../teachers/src/modules/event-bus/common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class StudentLeftClassToTeacherAgentEventInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: TeacherRoutingKey.studentLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
