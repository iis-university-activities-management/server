import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { StudentDto } from '../dtos';

export class StudentInfoResult {
  @ApiProperty({ type: StudentDto, nullable: true })
  data?: StudentDto;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
