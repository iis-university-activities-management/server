export * from './student-info.result';
export * from './get-profile.result';
export * from './upload-profile.result';
export * from './student-autobiography.result';
export * from './student-speciality.result';
