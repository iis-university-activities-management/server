import { ApiProperty } from '@nestjs/swagger';
import { AutobiographyDto } from '../dtos';

import { BaseException } from '@uam/exceptions';

export class GetStudentAutobiographyResult {
  @ApiProperty({ nullable: true, type: AutobiographyDto })
  data?: AutobiographyDto;

  @ApiProperty({ nullable: true, type: BaseException })
  error?: BaseException;
}
