import { ApiProperty } from '@nestjs/swagger';
import { ProfileDto } from '../dtos';

import { BaseException } from '@uam/exceptions';

export class UploadStudentProfileResult {
  @ApiProperty({ nullable: true, type: ProfileDto })
  data?: ProfileDto;

  @ApiProperty({ nullable: true, type: BaseException })
  error?: BaseException;
}
