import { ProfileDto } from '../dtos';

import { BaseException } from '@uam/exceptions';
import { ApiProperty } from '@nestjs/swagger';

export class GetStudentProfileResult {
  @ApiProperty({ nullable: true, type: ProfileDto })
  data?: ProfileDto;

  @ApiProperty({ nullable: true, type: BaseException })
  error?: BaseException;
}
