import { ApiProperty } from '@nestjs/swagger';
import { SpecialityDto } from '../dtos';

import { BaseException } from '@uam/exceptions';

export class GetStudentSpecialityResult {
  @ApiProperty({ nullable: true, type: SpecialityDto })
  data?: SpecialityDto;

  @ApiProperty({ nullable: true, type: BaseException })
  error?: BaseException;
}
