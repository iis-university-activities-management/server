import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOkResponse,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { GetUserCredentials, UserCredentials } from '@uam/auth';

import { CreateStudentInput } from './inputs';
import { StudentService } from '../application';
import {
  StudentInfoResult,
  UploadStudentProfileResult,
  GetStudentProfileResult,
  GetStudentAutobiographyResult,
  GetStudentSpecialityResult,
} from './results';
import { StudentEventInput } from './inputs/student-event.input';

@ApiTags('Students Info')
@Controller('v1/students')
export class StudentContoller {
  constructor(private readonly studentService: StudentService) {}

  @Get('/:id/info')
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiResponse({
    type: StudentInfoResult,
    description:
      'This endpoint returns complete student information by student ID. If the student was not found, it will return a 404 error',
  })
  async studentInfo(
    @Param('id') studentId: string,
  ): Promise<StudentInfoResult> {
    const data = await this.studentService.getStudentById({
      studentId: +studentId,
    });

    return {
      data,
    };
  }

  @Get('/:id/profile')
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiResponse({
    type: GetStudentProfileResult,
    description:
      'Returns a link to the photo, as well as the key under which it was saved. If the student was not found, it will return a 404 error',
  })
  async getProfile(
    @Param('id') studentId: string,
  ): Promise<UploadStudentProfileResult> {
    const profile = await this.studentService.getProfile({
      studentId: +studentId,
    });

    return {
      data: {
        ...profile,
      },
    };
  }

  @ApiConsumes('multipart/form-data')
  @ApiOkResponse({
    type: UploadStudentProfileResult,
    description:
      'Returns a link to the photo, as well as the key under which it was saved. If the student was not found, it will return a 404 error',
  })
  @ApiParam({ name: 'id' })
  @ApiBearerAuth()
  @ApiBody({
    description:
      'This endpoint allows you to upload a student profile photo. Photo can be in any format',
    schema: {
      nullable: false,
      type: 'object',
      required: ['profile'],
      properties: {
        profile: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Patch('/:id/profile')
  @UseInterceptors(FileInterceptor('profile'))
  async uploadProfile(
    @GetUserCredentials('http') userCredentials: UserCredentials,
    @Param('id') studentId: string,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<UploadStudentProfileResult> {
    const { key, uri } = await this.studentService.uploadProfile({
      profile: file,
      studentId: +studentId,
    });
    return {
      data: {
        key,
        uri,
      },
    };
  }

  @Get('/:id/autobiography')
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiResponse({
    type: GetStudentAutobiographyResult,
    description:
      'This endpoint returns student autobiography by student ID. If the student was not found, it will return a 404 error',
  })
  async studentAutobiography(
    @Param('id') studentId: string,
  ): Promise<GetStudentAutobiographyResult> {
    const data = await this.studentService.getStudentAutobiography({
      studentId: +studentId,
    });

    return {
      data,
    };
  }

  @Get('/:id/speciality')
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiResponse({
    type: GetStudentSpecialityResult,
    description:
      'This endpoint returns student speciality by student ID. If the student was not found, it will return a 404 error',
  })
  async studentSpeciality(
    @Param('id') studentId: string,
  ): Promise<GetStudentSpecialityResult> {
    const data = await this.studentService.getStudentSpecialty({
      studentId: +studentId,
    });

    return {
      data,
    };
  }

  @Post('/create')
  @ApiResponse({
    type: CreateStudentInput,
  })
  async createStudent(@Body() input: CreateStudentInput): Promise<any> {
    const data = await this.studentService.createStudent(input);

    return {};
  }

  @Post('/event')
  @ApiBearerAuth()
  async createLeftClassEvent(@Body() input: StudentEventInput) {
    await this.studentService.createLeftClassEvent({ ...input });
  }
}
