import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateStudentInput {
  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  middleName: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ type: Date, nullable: false })
  @IsString()
  @IsNotEmpty()
  birthDay: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  groupName: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  email: string;
}
