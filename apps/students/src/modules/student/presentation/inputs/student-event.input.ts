import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class StudentEventInput {
  @ApiProperty({ type: Number, nullable: false })
  @IsNumber()
  @IsNotEmpty()
  classId: number;

  @ApiProperty({ type: String, nullable: true })
  @IsString()
  @IsOptional()
  message?: string;

  @ApiProperty({ type: Number, nullable: false })
  @IsNumber()
  @IsNotEmpty()
  studentId: number;
}
