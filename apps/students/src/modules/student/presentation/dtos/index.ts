export * from './student.dto';
export * from './profile.dto';
export * from './autobiography.dto';
export * from './speciality.dto';
export * from './student-classes.dto';
