import { ApiProperty } from '@nestjs/swagger';

export class ProfileDto {
  @ApiProperty({ type: String })
  key: string;

  @ApiProperty({ type: String })
  uri: string;
}
