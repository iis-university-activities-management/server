import { ApiProperty } from '@nestjs/swagger';

export class AutobiographyDto {
  @ApiProperty({ type: String })
  text: string;
}
