import { ApiProperty } from '@nestjs/swagger';

export class StudentDto {
  @ApiProperty({ type: String, nullable: false })
  id: number;

  @ApiProperty({ type: String, nullable: false })
  firstName: string;

  @ApiProperty({ type: String, nullable: false })
  middleName: string;

  @ApiProperty({ type: String, nullable: false })
  lastName: string;

  @ApiProperty({ type: Date, nullable: false })
  birthDay: string;
}
