import { ApiProperty } from '@nestjs/swagger';

export class SpecialityDto {
  @ApiProperty({ type: String })
  speciality: string;

  @ApiProperty({ type: Number })
  course: number;

  @ApiProperty({ type: String })
  admissionAt: string;
}
