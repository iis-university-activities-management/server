import {
  Resolver,
  Query,
  Args,
  Parent,
  ResolveField,
  Mutation,
  PartialType,
  InputType,
} from '@nestjs/graphql';
import { StudentService } from '../application';
import { Field, Int, ObjectType } from '@nestjs/graphql';

import {
  BadRequestException,
  BaseError,
  GqlExceptionFilter,
} from '@uam/exceptions';
import { UseFilters } from '@nestjs/common';

@ObjectType()
export class Post {
  @Field((type) => Int)
  id: number;

  @Field()
  title: string;

  @Field((type) => Int, { nullable: true })
  votes?: number;
}

@ObjectType()
export class Author {
  @Field((type) => Int)
  id: number;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field((type) => [Post])
  posts: Post[];
}

@ObjectType()
class Res {
  @Field(() => Author, { nullable: true })
  data?: Author;

  @Field(() => BaseError, { nullable: true })
  error?: BaseError;
}

@InputType()
export class CreateAuthorInput {
  @Field(() => String, { nullable: true })
  firstName?: string;

  @Field(() => String, { nullable: true })
  lastName?: string;
}

@UseFilters(GqlExceptionFilter)
@Resolver((of) => Author)
export class StudentResolver {
  @Query((returns) => Author)
  async author(@Args('id', { type: () => Int }) id: number) {
    return {
      id: 2,
    };
  }

  @ResolveField()
  async posts(@Parent() author: Author) {
    return [];
  }

  @ResolveField()
  async firstName(@Parent() author: Author) {
    return 'MyFirstName';
  }

  @ResolveField()
  async lastName(@Parent() author: Author) {
    return 'MyLastName';
  }

  @Mutation(() => Res)
  async createAuthor(@Args('author') author: CreateAuthorInput): Promise<Res> {
    const newAuthor = new Author();
    return {
      data: {
        id: 12,
        posts: [],
        firstName: 'some 1',
        lastName: 'some 2',
      },
    };
  }
}
