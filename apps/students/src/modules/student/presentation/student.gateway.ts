import { WebSocketGateway } from '@nestjs/websockets';
import { StudentService } from '../application';

@WebSocketGateway({ namespace: 'students' })
export class StudentWsGateway {
  constructor(private readonly studentService: StudentService) {}
}
