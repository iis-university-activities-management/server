import { Injectable } from '@nestjs/common';
import { AwsS3Service } from '@uam/aws/s3';
import {
  AutobiographyRepository,
  FileRepository,
  SpecialityRepository,
} from '@uam/global';
import { BadRequestException } from '@uam/exceptions';

import { StudentDomain } from '../domain';
import {
  Student,
  StudentProfileRelationRepository,
  StudentRepository,
  StudentAutobiographyRelationRepository,
  StudentSpecialtyRelationRepository,
} from '../infrastructure';
import {
  CreateStudentParameters,
  GetProfileParameters,
  GetProfileResult,
  GetStudentAutobiographyParameters,
  GetStudentAutobiographyResult,
  GetStudentByIdParameters,
  GetStudentSpecialtyParameters,
  GetStudentSpecialtyResult,
  UploadProfileParameters,
  UploadProfileResult,
} from './student-service.type';
import { StudentCreateEvent } from '../../event-bus/outbound/events/students-create.event';
import { StudentLeftClassEvent } from '../../event-bus/outbound/events/student-left-class.event';

@Injectable()
export class StudentService {
  constructor(
    private readonly studentDomain: StudentDomain,
    private readonly studentRepository: StudentRepository,
    private readonly profileRepository: FileRepository,
    private readonly specialityRepository: SpecialityRepository,
    private readonly autobiographyRepository: AutobiographyRepository,
    private readonly studentProfileRepository: StudentProfileRelationRepository,
    private readonly studentSpecialtyRepository: StudentSpecialtyRelationRepository,
    private readonly studentAutobiographyRelationRepository: StudentAutobiographyRelationRepository,
    private readonly awsS3Service: AwsS3Service,
    private readonly createStudentEvent: StudentCreateEvent,
    private readonly studentLeftClassEvent: StudentLeftClassEvent,
  ) {}

  async uploadProfile(
    params: UploadProfileParameters,
  ): Promise<UploadProfileResult> {
    const { profile, studentId } = params;

    const result = await this.awsS3Service.putFile({
      file: profile,
      name: profile.filename,
      subPath: 'students-profiles/',
    });

    const { key, uri } = await this.awsS3Service.getFileUri({
      key: result.key,
    });

    const file = await this.profileRepository.save({ key, uri });

    await this.studentProfileRepository.saveByIds(
      studentId,
      {
        addedAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      },
      file.id,
    );

    return {
      key,
      uri,
    };
  }

  async getStudentById(params: GetStudentByIdParameters): Promise<Student> {
    const { studentId } = params;

    const student = await this.studentRepository.getById(studentId);

    if (!student) {
      throw new BadRequestException(
        'getStudentById',
        `student by id <${studentId}> not found`,
      );
    }

    return student;
  }

  async getProfile(params: GetProfileParameters): Promise<GetProfileResult> {
    const { studentId } = params;

    const file = await this.studentProfileRepository.getRightLabel(studentId);

    return {
      key: file.key,
      uri: file.uri,
    };
  }

  async getStudentAutobiography(
    params: GetStudentAutobiographyParameters,
  ): Promise<GetStudentAutobiographyResult> {
    const { studentId } = params;
    const autobiography =
      await this.studentAutobiographyRelationRepository.getRightLabel(
        studentId,
      );

    return autobiography;
  }

  async getStudentSpecialty(
    params: GetStudentSpecialtyParameters,
  ): Promise<GetStudentSpecialtyResult> {
    const { studentId } = params;

    const speciality = await this.studentSpecialtyRepository.getRightLabel(
      studentId,
    );

    const relation = await this.studentSpecialtyRepository.getRelationByIds(
      studentId,
      speciality.id,
    );

    return {
      admissionAt: relation.admissionAt,
      course: relation.course,
      speciality: speciality.name,
    };
  }

  async createStudent(params: CreateStudentParameters) {
    await this.createStudentEvent.execute(params);
    // const student = await this.studentRepository.save({
    //   birthDay,
    //   firstName,
    //   lastName,
    //   middleName,
    // });
    // const speciality = await this.specialityRepository.save({
    //   name: 'Промышленная электроника',
    // });
    // const speciality2 = await this.specialityRepository.save({
    //   name: 'Автоматизированные системы обработки информации',
    // });
    // const speciality3 = await this.specialityRepository.save({
    //   name: 'Информационные технологии и управление в технических системах',
    // });
    // const relation = await this.studentSpecialtyRepository.saveByIds(
    //   0,
    //   { admissionAt: new Date().toISOString(), course: 3 },
    //   speciality.id,
    // );
    // console.log(speciality, speciality2, speciality3);
  }

  async createLeftClassEvent(params: {
    message?: string;
    classId: number;
    studentId: number;
  }) {
    await this.studentLeftClassEvent.execute(params);
  }
}
