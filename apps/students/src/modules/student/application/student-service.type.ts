export interface UploadProfileParameters {
  studentId: number;
  profile: Express.Multer.File;
}
export interface UploadProfileResult {
  uri: string;
  key: string;
}

export interface GetProfileParameters {
  studentId: number;
}
export interface GetProfileResult {
  uri: string;
  key: string;
}

export interface GetStudentByIdParameters {
  studentId: number;
}

export interface GetStudentAutobiographyParameters {
  studentId: number;
}
export interface GetStudentAutobiographyResult {
  text: string;
}

export interface GetStudentSpecialtyParameters {
  studentId: number;
}
export interface GetStudentSpecialtyResult {
  speciality: string;
  admissionAt: string;
  course: number;
}

export interface CreateStudentParameters {
  firstName: string;
  middleName: string;
  lastName: string;
  birthDay: string;
  groupName: string;
  email: string;
}
