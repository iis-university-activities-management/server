import { Module } from '@nestjs/common';

import { Neo4jModule } from '@uam/neo4j';
import {
  AutobiographyRepository,
  FileRepository,
  SpecialityRepository,
} from '@uam/global';

import { Neo4jConfigFactory } from '../../config';
import { InboundModule, OutboundModule } from '../event-bus';
import { StudentService } from './application';
import { StudentDomain } from './domain';
import {
  StudentProfileRelationRepository,
  StudentRepository,
  StudentAutobiographyRelationRepository,
  StudentSpecialtyRelation,
  StudentSpecialtyRelationRepository,
} from './infrastructure';
import {
  StudentContoller,
  StudentWsGateway,
  StudentResolver,
  StudentHandler,
} from './presentation';

@Module({
  imports: [
    OutboundModule,
    InboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [
    StudentDomain,
    StudentService,
    StudentWsGateway,
    StudentHandler,
    StudentResolver,
    StudentRepository,
    FileRepository,
    SpecialityRepository,
    AutobiographyRepository,
    StudentSpecialtyRelation,
    StudentAutobiographyRelationRepository,
    StudentSpecialtyRelationRepository,
    StudentProfileRelationRepository,
  ],
  controllers: [StudentContoller],
})
export class StudentModule {}
