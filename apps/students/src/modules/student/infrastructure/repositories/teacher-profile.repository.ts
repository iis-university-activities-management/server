import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { File } from '@uam/global';
import { StudentProfileRelation } from '../entities';
import { Teacher } from '../../../../../../teachers/src/modules/teacher/infrastructure';

@Injectable()
export class TeacherProfileRelationRepository extends Neo4jRelationRepository<
  StudentProfileRelation,
  Teacher,
  File
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentProfileRelation, Teacher, File, neo4jService);
  }
}
