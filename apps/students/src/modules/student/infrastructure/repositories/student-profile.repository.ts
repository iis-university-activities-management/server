import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { File } from '@uam/global';
import { StudentProfileRelation, Student } from '../entities';

@Injectable()
export class StudentProfileRelationRepository extends Neo4jRelationRepository<
  StudentProfileRelation,
  Student,
  File
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentProfileRelation, Student, File, neo4jService);
  }
}
