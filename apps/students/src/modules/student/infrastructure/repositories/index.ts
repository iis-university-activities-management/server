export * from './student.repository';
export * from './student-profile.repository';
export * from './student-autobiography.repository';
export * from './student-specialty.repository';
