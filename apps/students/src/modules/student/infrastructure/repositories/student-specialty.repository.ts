import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Autobiography, Specialty } from '@uam/global';
import { StudentSpecialtyRelation, Student } from '../entities';

@Injectable()
export class StudentSpecialtyRelationRepository extends Neo4jRelationRepository<
  StudentSpecialtyRelation,
  Student,
  Specialty
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentSpecialtyRelation, Student, Specialty, neo4jService);
  }
}
