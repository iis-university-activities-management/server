import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Autobiography } from '@uam/global';
import { StudentAutobiographyRelation, Student } from '../entities';

@Injectable()
export class StudentAutobiographyRelationRepository extends Neo4jRelationRepository<
  StudentAutobiographyRelation,
  Student,
  Autobiography
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentAutobiographyRelation, Student, Autobiography, neo4jService);
  }
}
