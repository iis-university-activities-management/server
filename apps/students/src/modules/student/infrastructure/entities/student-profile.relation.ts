import { PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';

import { IStudentProfileRelation } from '../../domain';

@Relation()
export class StudentProfileRelation implements IStudentProfileRelation {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  addedAt: string;

  @Property()
  updatedAt: string;
}
