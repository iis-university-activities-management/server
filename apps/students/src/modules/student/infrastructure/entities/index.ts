export * from './student-profile.relation';
export * from './student.label';
export * from './student-autobiography.relation';
export * from './student-specialty.relation';
