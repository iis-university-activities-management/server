import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { IStudent } from '../../domain';

@Label()
export class Student implements IStudent {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  firstName: string;

  @Property()
  middleName: string;

  @Property()
  lastName: string;

  @Property()
  birthDay: string;

  @Property()
  email: string;
}
