import { PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';

import { IStudentSpecialtyRelation } from '../../domain';

@Relation()
export class StudentSpecialtyRelation implements IStudentSpecialtyRelation {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  course: number;

  @Property()
  admissionAt: string;
}
