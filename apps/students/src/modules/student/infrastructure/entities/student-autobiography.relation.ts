import { PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';

import { IStudentAutobiographyRelation } from '../../domain';

@Relation()
export class StudentAutobiographyRelation
  implements IStudentAutobiographyRelation
{
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  addedAt: string;

  @Property()
  updatedAt: string;
}
