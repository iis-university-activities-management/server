export interface IStudentSpecialtyRelation {
  id: number;
  admissionAt: string;
  course: number;
}
