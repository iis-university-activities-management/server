export interface IStudentAutobiographyRelation {
  id: number;
  addedAt: string;
  updatedAt: string;
}
