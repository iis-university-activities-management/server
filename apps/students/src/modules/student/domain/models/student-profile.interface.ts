export interface IStudentProfileRelation {
  id: number;
  addedAt: string;
  updatedAt: string;
}
