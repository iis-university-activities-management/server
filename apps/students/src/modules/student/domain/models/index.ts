export * from './student-profile.interface';
export * from './student-specialty.interface';
export * from './student.interface';
export * from './student-autobiography.interface';
