export interface IStudent {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  birthDay: string;
  email: string;
}
