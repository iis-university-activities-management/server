export interface IStudentPrivacy {
  id: number;
  isPrivate: boolean;
}
