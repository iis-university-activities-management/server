import { Module } from '@nestjs/common';

import { Neo4jModule } from '@uam/neo4j';
import { AuthCredentialsRepository } from '@uam/global';

import { JwtConfigFactory, Neo4jConfigFactory } from '../../config';
import { InboundModule, OutboundModule } from '../event-bus';
import {
  StudentAuthCredentialsRelationRepository,
  StudentPrivacyRelationRepository,
  StudentPrivacyRepository,
} from './infrastructure';
import {
  StudentAuthContoller,
  StudentConfidentialityContoller,
} from './presentation';
import { StudentAuthService } from './application';
import { JwtModule } from '@nestjs/jwt';
import { GroupHasStudentRelationRepository } from '../../../../groups/src/modules/group/infrastructure';
import { StudentProfileRelationRepository } from '../student/infrastructure';

@Module({
  imports: [
    OutboundModule,
    InboundModule,
    JwtModule.registerAsync({ useClass: JwtConfigFactory }),
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [
    StudentPrivacyRepository,
    StudentPrivacyRelationRepository,
    AuthCredentialsRepository,
    StudentAuthCredentialsRelationRepository,
    GroupHasStudentRelationRepository,
    StudentProfileRelationRepository,
    StudentAuthService,
  ],
  controllers: [StudentAuthContoller, StudentConfidentialityContoller],
})
export class StudentAuthModule {}
