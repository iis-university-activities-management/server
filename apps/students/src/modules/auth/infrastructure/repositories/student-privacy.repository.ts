import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { StudentPrivacy } from '../entities';

@Injectable()
export class StudentPrivacyRepository extends Neo4jLabelRepository<StudentPrivacy> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentPrivacy, neo4jService);
  }
}
