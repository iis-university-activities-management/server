import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Student } from '../../../student/infrastructure';
import { StudentPrivacy, StudentPrivacyRelation } from '../entities';

@Injectable()
export class StudentPrivacyRelationRepository extends Neo4jRelationRepository<
  StudentPrivacyRelation,
  Student,
  StudentPrivacy
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(StudentPrivacyRelation, Student, StudentPrivacy, neo4jService);
  }
}
