import { Inject, Injectable } from '@nestjs/common';

import { NEO4J_CONNECTION, Neo4jConnection } from '@uam/neo4j';
import { AuthCredentialsRelationRepository } from '@uam/global';

import { Student } from '../../../student/infrastructure';

@Injectable()
export class StudentAuthCredentialsRelationRepository extends AuthCredentialsRelationRepository<Student> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Student, neo4jService);
  }
}
