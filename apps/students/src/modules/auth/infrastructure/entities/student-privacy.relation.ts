import { Label, PrimaryGeneratedColumn } from '@uam/neo4j';

import { IStudentPrivacyRelation } from '../../domain';

@Label()
export class StudentPrivacyRelation implements IStudentPrivacyRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
