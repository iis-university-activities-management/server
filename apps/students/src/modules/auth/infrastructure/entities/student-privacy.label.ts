import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { IStudentPrivacy } from '../../domain';

@Label()
export class StudentPrivacy implements IStudentPrivacy {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  isPrivate: boolean;
}
