import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '@uam/auth';
import { AuthCredentialsRepository } from '@uam/global';
import { UnauthorizedException } from '@uam/exceptions';

import { Config } from '../../../config';
import { StudentAuthCredentialsRelationRepository } from '../infrastructure';
import {
  GenerateTokenParams,
  SignInParams,
  SignInResult,
} from './auth-service.type';
import { GroupHasStudentRelationRepository } from '../../../../../groups/src/modules/group/infrastructure';
import { StudentProfileRelationRepository } from '../../student/infrastructure';

@Injectable()
export class StudentAuthService {
  constructor(
    private readonly studentCredentialsRepository: AuthCredentialsRepository,
    private readonly studentCredentialsRelationRepository: StudentAuthCredentialsRelationRepository,
    private readonly groupHasStudentRelationRepository: GroupHasStudentRelationRepository,
    private readonly studentProfileRelationRepository: StudentProfileRelationRepository,
    private readonly configService: ConfigService<Config>,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(params: SignInParams): Promise<SignInResult> {
    const { login, password } = params;
    const credentials = await this.studentCredentialsRepository.getByLogin(
      login,
    );

    if (!credentials) {
      throw new UnauthorizedException(
        `Teacher with this login not found: <${login}>`,
      );
    }

    if (password !== credentials.password) {
      throw new UnauthorizedException(`Login or password is incorrect`);
    }

    const student =
      await this.studentCredentialsRelationRepository.getLeftLabel(
        credentials.id,
      );
    const group = await this.groupHasStudentRelationRepository.getLeftLabel(
      student.id,
    );

    const profile = await this.studentProfileRelationRepository.getRightLabel(
      student.id,
    );

    return {
      data: { ...student, group, profile },
    };
  }

  // async create() {
  //   const credentials = await this.studentCredentialsRepository.save({
  //     login: 'andrei',
  //     password: 'andrei',
  //   });

  //   const relation = await this.studentCredentialsRelationRepository.saveByIds(
  //     0,
  //     {
  //       addedAt: new Date().toISOString(),
  //       updatedAt: new Date().toISOString(),
  //     },
  //     credentials.id,
  //   );

  //   console.log(credentials, relation);
  // }

  private async generateAccessToken(
    params: GenerateTokenParams,
  ): Promise<string> {
    const { studentId } = params;

    const { accessTokenExpiresIn, accessTokenSecret } =
      this.configService.get('jwt');

    const payload: JwtPayload = { id: studentId };

    return this.jwtService.signAsync(payload, {
      expiresIn: accessTokenExpiresIn,
      secret: accessTokenSecret,
    });
  }

  private async generateRefreshToken(
    params: GenerateTokenParams,
  ): Promise<string> {
    const { studentId } = params;

    const { refreshTokenExpiresIn, refreshTokenSecret } =
      this.configService.get('jwt');

    const payload: JwtPayload = { id: studentId };

    return this.jwtService.signAsync(payload, {
      expiresIn: refreshTokenExpiresIn,
      secret: refreshTokenSecret,
    });
  }
}
