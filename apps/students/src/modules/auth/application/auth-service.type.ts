import { IStudent } from '../../student/domain';

export interface GenerateTokenParams {
  studentId: number;
}

export interface SignInParams {
  login: string;
  password: string;
}
export interface SignInResult {
  data: any;
}
