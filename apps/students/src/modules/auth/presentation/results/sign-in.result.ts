import { ApiProperty } from '@nestjs/swagger';

import { BaseException } from '@uam/exceptions';
import { StudentDto } from '../../../student/presentation/dtos';

export class StudentSingInResult {
  @ApiProperty({ nullable: false, type: StudentDto })
  data: StudentDto;

  @ApiProperty({ nullable: true, type: BaseException })
  error?: BaseException;
}
