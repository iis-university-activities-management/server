import { ApiProperty } from '@nestjs/swagger';

export class TokensDto {
  @ApiProperty({ nullable: false, type: String })
  accessToken: string;

  @ApiProperty({ nullable: false, type: String })
  refreshToken: string;
}
