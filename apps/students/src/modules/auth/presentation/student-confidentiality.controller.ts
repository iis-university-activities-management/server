import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { StudentAuthService } from '../application';

@ApiTags('Confidentiality')
@Controller('v1/confidentiality')
export class StudentConfidentialityContoller {
  constructor(private readonly authService: StudentAuthService) {}
}
