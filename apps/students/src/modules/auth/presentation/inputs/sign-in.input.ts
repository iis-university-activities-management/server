import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class StudentSignInInput {
  @ApiProperty({ nullable: false, type: String })
  @IsString()
  @IsNotEmpty()
  login: string;

  @ApiProperty({ nullable: false, type: String })
  @IsString()
  @IsNotEmpty()
  password: string;
}
