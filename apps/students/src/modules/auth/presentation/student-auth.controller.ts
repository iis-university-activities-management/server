import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { StudentAuthService } from '../application';
import { StudentSignInInput } from './inputs';
import { StudentSingInResult } from './results/sign-in.result';

@ApiTags('Student Auth')
@Controller('v1/auth')
export class StudentAuthContoller {
  constructor(private readonly authService: StudentAuthService) {}

  @ApiOkResponse({
    type: StudentSingInResult,
    description:
      'Returns a link to the photo, as well as the key under which it was saved. If the student was not found, it will return a 404 error',
  })
  @Post('/sign-in')
  async signIn(
    @Body() signInInput: StudentSignInInput,
  ): Promise<StudentSingInResult> {
    const { data } = await this.authService.signIn(signInInput);
    return { data };
  }

  // @Get('/sign-in')
  // async signIn2() {
  //   return this.authService.create();
  // }
}
