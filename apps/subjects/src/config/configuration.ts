import { applicationConfiguration } from './application';
import { awsS3Configuration } from './aws/s3';
import { graphqlConfiguration } from './graphql';
import { jwtConfiguration } from './jwt';
import { neo4jConfiguration } from './neo4j';

export const configuration = [
  applicationConfiguration,
  jwtConfiguration,
  graphqlConfiguration,
  neo4jConfiguration,
  awsS3Configuration,
];
