import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class EnvSchema {
  @IsNotEmpty()
  @IsNumber()
  SUBJECTS_PORT: number;

  @IsNotEmpty()
  @IsString()
  SUBJECTS_HOST: string;

  @IsNotEmpty()
  @IsString()
  AWS_S3_SUBJECTS_BUCKET: string;

  @IsNotEmpty()
  @IsString()
  AWS_REGION: string;

  @IsNotEmpty()
  @IsString()
  AWS_ACCESS_KEY: string;

  @IsNotEmpty()
  @IsString()
  AWS_SECRET_ACCESS_KEY: string;

  @IsNotEmpty()
  @IsString()
  AWS_S3_EXPIRES_IN: string;

  @IsNotEmpty()
  @IsNumber()
  MINIO_PORT: number;

  @IsNotEmpty()
  @IsNumber()
  MINIO_PORT_UI: number;

  @IsNotEmpty()
  @IsString()
  JWT_ACCESS_TOKEN_SECRET: string;

  @IsNotEmpty()
  @IsString()
  JWT_REFRESH_TOKEN_SECRET: string;

  @IsNotEmpty()
  @IsNumber()
  JWT_ACCESS_TOKEN_EXPIRES_IN: number;

  @IsNotEmpty()
  @IsNumber()
  JWT_REFRESH_TOKEN_EXPIRES_IN: number;

  @IsNotEmpty()
  @IsString()
  NEO4J_USERNAME: string;

  @IsNotEmpty()
  @IsString()
  NEO4J_PASSWORD: string;

  @IsNotEmpty()
  @IsString()
  NEO4J_DEBUG_LEVEL: string;

  @IsNotEmpty()
  @IsNumber()
  NEO4J_PORT_UI: number;

  @IsNotEmpty()
  @IsNumber()
  NEO4J_PORT: number;

  @IsNotEmpty()
  @IsString()
  NEO4J_HOST: string;

  @IsNotEmpty()
  @IsString()
  NEO4J_DATABASE: string;

  @IsNotEmpty()
  @IsNumber()
  RABBIT_MQ_PORT: number;

  @IsNotEmpty()
  @IsNumber()
  RABBIT_MQ_PORT_UI: number;

  @IsNotEmpty()
  @IsString()
  RABBIT_MQ_HOST: string;

  @IsNotEmpty()
  @IsString()
  RABBIT_MQ_USERNAME: string;

  @IsNotEmpty()
  @IsString()
  RABBIT_MQ_PASSWORS: string;
}
