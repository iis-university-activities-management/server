import { ApplicationConfig } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfig => ({
  application: {
    port: Number(process.env.SUBJECTS_PORT),
    host: process.env.SUBJECTS_HOST,
  },
});
