import { Module } from '@nestjs/common';
import { OutboundModule } from '../outbound';

@Module({
  imports: [OutboundModule],
})
export class InboundModule {}
