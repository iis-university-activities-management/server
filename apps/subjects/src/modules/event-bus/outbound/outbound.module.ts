import { Module } from '@nestjs/common';
import { SubjectCreatedEvent } from './events/subject-created.event';

@Module({
  providers: [SubjectCreatedEvent],
  exports: [SubjectCreatedEvent],
})
export class OutboundModule {}
