import { ConsumeMessage } from 'amqplib';
import { SubjectRoutingKey } from '../../common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class SubjectCreatedEventInterface {
  static config = {
    exchange: Exchange.topic,
    routingKey: SubjectRoutingKey.studentCreatedEvent,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
