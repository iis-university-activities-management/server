import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { SubjectCreatedEventInterface } from './subject-created-event.interface';

@Injectable()
export class SubjectCreatedEvent implements SubjectCreatedEventInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = SubjectCreatedEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
