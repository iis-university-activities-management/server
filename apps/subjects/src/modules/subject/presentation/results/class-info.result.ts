import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { ClassDto } from '../dtos';

export class CreateClassResult {
  @ApiProperty({ type: ClassDto, nullable: true })
  data?: ClassDto;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
