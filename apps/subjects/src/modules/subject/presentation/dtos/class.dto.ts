import { ApiProperty } from '@nestjs/swagger';

export class ClassDto {
  @ApiProperty({ type: String, nullable: false })
  dayOfTheWeek: string;

  @ApiProperty({ type: String, nullable: false })
  ends: string;

  @ApiProperty({ type: String, nullable: false })
  name: string;

  @ApiProperty({ type: String, nullable: false })
  starts: string;

  @ApiProperty({ type: String, nullable: false })
  type: string;
}
