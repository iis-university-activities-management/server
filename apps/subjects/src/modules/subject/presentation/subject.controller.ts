import { Body, Controller, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

import { SubjectService } from '../application';
import { CreateClassInput } from './inputs/create-class.input';
import { CreateClassResult } from './results';

@ApiTags('Subjects Info')
@Controller('v1/subjects')
export class SubjectContoller {
  constructor(private readonly subjectService: SubjectService) {}

  @Post()
  @ApiBearerAuth()
  @ApiResponse({
    type: CreateClassResult,
  })
  async subjectInfo(
    @Body() input: CreateClassInput,
  ): Promise<CreateClassResult> {
    const { data } = await this.subjectService.createClass(input);

    return {
      data,
    };
  }
}
