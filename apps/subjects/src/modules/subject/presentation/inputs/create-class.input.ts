import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateClassInput {
  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  dayOfTheWeek: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  ends: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  starts: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  type: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  subjectName: string;

  @ApiProperty({ type: String, nullable: false })
  @IsString()
  @IsNotEmpty()
  group: string;

  @ApiProperty({ type: String, nullable: false })
  @IsNumber()
  @IsNotEmpty()
  subgroup: number;
}
