import { WebSocketGateway } from '@nestjs/websockets';
import { SubjectService } from '../application';

@WebSocketGateway({ namespace: 'subjects' })
export class SubjectWsGateway {
  constructor(private readonly studentService: SubjectService) {}
}
