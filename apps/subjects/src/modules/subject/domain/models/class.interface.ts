export interface IClass {
  dayOfTheWeek: string;
  ends: string;
  name: string;
  starts: string;
  type: string;
}
