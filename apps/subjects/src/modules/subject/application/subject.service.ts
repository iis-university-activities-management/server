import { Injectable } from '@nestjs/common';
import { AwsS3Service } from '@uam/aws/s3';
import { BadRequestException } from '@uam/exceptions';

import { SubjectDomain } from '../domain';
import {
  Subject,
  SubjectRepository,
  ClassRepository,
  ClassSubgroupRelationRepository,
  ClassSubjectRelationRepository,
} from '../infrastructure';
import { CreateClassResult } from '../presentation/results';
import {
  CreateClassParameters,
  GetSubjectByIdParameters,
} from './subject-service.type';

@Injectable()
export class SubjectService {
  constructor(
    private readonly subjectRepository: SubjectRepository,
    private readonly classRepository: ClassRepository,
    private readonly classSubjectRelation: ClassSubjectRelationRepository,
    private readonly classSubgroupRelationRepository: ClassSubgroupRelationRepository,
  ) {}

  async getSubjectById(params: GetSubjectByIdParameters): Promise<Subject> {
    const { subjectId } = params;

    const subject = await this.subjectRepository.getById(subjectId);

    if (!subject) {
      throw new BadRequestException(
        'getSubjectById',
        `subject by id <${subjectId}> not found`,
      );
    }

    return subject;
  }

  async createClass(params: CreateClassParameters): Promise<CreateClassResult> {
    const { subjectName, dayOfTheWeek, ends, name, starts, type } = params;

    const subject = await this.subjectRepository.getSubjectByName(subjectName);

    if (!subject) {
      throw new BadRequestException(
        `Create new class: subject with name <${subjectName}> not found`,
      );
    }

    const newClass = await this.classRepository.save({
      dayOfTheWeek,
      ends,
      name,
      starts,
      type,
    });

    const relation = await this.classSubjectRelation.saveByIds(
      newClass.id,
      {},
      subject.id,
    );

    console.log(relation);

    return {
      data: newClass,
    };
  }
}
