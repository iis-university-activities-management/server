import { IClass } from '../domain';

export interface UploadProfileParameters {
  subjectId: number;
  profile: Express.Multer.File;
}
export interface UploadProfileResult {
  uri: string;
  key: string;
}

export interface GetProfileParameters {
  subjectId: number;
}
export interface GetProfileResult {
  uri: string;
  key: string;
}

export interface GetSubjectByIdParameters {
  subjectId: number;
}

export interface GetSubjectAutobiographyParameters {
  subjectId: number;
}
export interface GetSubjectAutobiographyResult {
  text: string;
}

export interface GetSubjectSpecialtyParameters {
  subjectId: number;
}
export interface GetSubjectSpecialtyResult {
  speciality: string;
  admissionAt: string;
  course: number;
}

export interface CreateClassResult {
  data: IClass;
}

export interface CreateClassParameters {
  dayOfTheWeek: string;
  ends: string;
  name: string;
  starts: string;
  type: string;
  subjectName: string;
  group: string;
  subgroup: number;
}
