import { Module } from '@nestjs/common';

import { Neo4jModule } from '@uam/neo4j';
import { FileRepository } from '@uam/global';

import { Neo4jConfigFactory } from '../../config';
import { InboundModule, OutboundModule } from '../event-bus';
import { SubjectService } from './application';
import { SubjectDomain } from './domain';
import {
  ClassRepository,
  SubjectRepository,
  ClassSubjectRelationRepository,
  ClassSubgroupRelationRepository,
} from './infrastructure';
import {
  SubjectContoller,
  SubjectWsGateway,
  SubjectResolver,
  SubjectHandler,
} from './presentation';

@Module({
  imports: [
    OutboundModule,
    InboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [
    SubjectDomain,
    SubjectService,
    SubjectWsGateway,
    SubjectHandler,
    SubjectResolver,
    SubjectRepository,
    ClassRepository,
    ClassSubjectRelationRepository,
    ClassSubgroupRelationRepository,
    FileRepository,
  ],
  controllers: [SubjectContoller],
})
export class SubjectModule {}
