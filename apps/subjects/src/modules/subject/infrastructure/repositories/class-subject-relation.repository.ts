import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';

import { ClassSubjectRelation, Class, Subject } from '../entities';

@Injectable()
export class ClassSubjectRelationRepository extends Neo4jRelationRepository<
  ClassSubjectRelation,
  Class,
  Subject
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(ClassSubjectRelation, Class, Subject, neo4jService);
  }
}
