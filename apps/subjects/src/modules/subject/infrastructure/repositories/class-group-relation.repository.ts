import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Subgroup } from '../../../../../../groups/src/modules/group/infrastructure';

import { Class, ClassSubgroupRelation } from '../entities';

@Injectable()
export class ClassSubgroupRelationRepository extends Neo4jRelationRepository<
  ClassSubgroupRelation,
  Class,
  Subgroup
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(ClassSubgroupRelation, Class, Subgroup, neo4jService);
  }
}
