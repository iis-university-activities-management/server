import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Subject } from '../entities';

@Injectable()
export class SubjectRepository extends Neo4jLabelRepository<Subject> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Subject, neo4jService);
  }

  async getSubjectByName(name: string): Promise<Subject> {
    const classMetadata = this.getClassMetadata();

    const query = `MATCH (n:${classMetadata.name}) WHERE n.name=$name  RETURN n`;
    const params = { name };

    const result = await this.neo4jConnection.read(query, params, 'neo4j');

    return this.convertResultToEntity(result) as any;
  }
}
