import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { IClass } from '../../domain';

@Label()
export class Class implements IClass {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  type: string;

  @Property()
  dayOfTheWeek: string;

  @Property()
  ends: string;

  @Property()
  starts: string;

  @Property()
  name: string;
}
