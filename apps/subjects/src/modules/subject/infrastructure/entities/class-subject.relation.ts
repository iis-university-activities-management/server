import { Label, PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';
import { IClassSubjectRelation } from '../../domain';

@Relation()
export class ClassSubjectRelation implements IClassSubjectRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
