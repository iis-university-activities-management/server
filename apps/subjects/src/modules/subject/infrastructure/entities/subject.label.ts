import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { ISubject } from '../../domain';

@Label()
export class Subject implements ISubject {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  name: string;
}
