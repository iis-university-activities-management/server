import { Label, PrimaryGeneratedColumn, Property, Relation } from '@uam/neo4j';
import { IClassSubgroupRelation } from '../../domain';

@Relation()
export class ClassSubgroupRelation implements IClassSubgroupRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
