import { Global, Module } from '@nestjs/common';
import { RabbitMQConfig, RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Config } from '../../config';

@Module({
  exports: [RabbitMQModule],
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<Config>): RabbitMQConfig => {
        const port = Number(process.env.RABBIT_MQ_PORT);
        const host = process.env.RABBIT_MQ_HOST;
        const username = process.env.RABBIT_MQ_USERNAME;
        const password = process.env.RABBIT_MQ_PASSWORS;

        // console.log({ uri: `amqp://${username}:${password}@${host}:${port}` });

        return {
          uri: `amqp://${username}:${password}@${host}:${port}`,
        };
      },
    }),
  ],
})
@Global()
export class AmqpModule {}
