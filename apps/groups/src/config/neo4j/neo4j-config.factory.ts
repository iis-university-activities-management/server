import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Neo4jOptions, Neo4jModuleOptionsFactory } from '@uam/neo4j';

import { Config } from '../config.interface';

@Injectable()
export class Neo4jConfigFactory implements Neo4jModuleOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  useNeo4jOptionsFactory(): Neo4jOptions {
    const { host, password, port, scheme, username, database } =
      this.configService.get('neo4j');

    return {
      host,
      password,
      port,
      scheme,
      username,
      database,
    };
  }
}
