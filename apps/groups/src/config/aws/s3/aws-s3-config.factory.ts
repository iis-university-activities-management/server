import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { AwsS3ModuleOptions, AwsS3ModuleOptionsFactory } from '@uam/aws/s3';

import { AwsS3Config } from './aws-s3-config.interface';

@Injectable()
export class AwsS3ConfigFactory implements AwsS3ModuleOptionsFactory {
  constructor(private readonly configService: ConfigService<AwsS3Config>) {}

  useAwsS3OptionsFactory(): AwsS3ModuleOptions {
    const {
      accessKeyId,
      bucket,
      endpoint,
      region,
      secretAccessKey,
      expiresIn,
    } = this.configService.get('awsS3');

    return {
      acl: 'private',
      bucketName: bucket,
      endpoint,
      apiVersion: 'latest',
      credentials: {
        accessKeyId,
        secretAccessKey,
      },
      region,
      forcePathStyle: true, // for MINIO
      expiresIn,
    };
  }
}
