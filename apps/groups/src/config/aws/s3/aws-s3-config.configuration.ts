import { AwsS3Config } from './aws-s3-config.interface';

export const awsS3Configuration = (): AwsS3Config => ({
  awsS3: {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucket: process.env.AWS_S3_STUDENTS_BUCKET,
    endpoint: `http://${process.env.AWS_HOST}:${process.env.MINIO_PORT}/`,
    region: process.env.AWS_REGION,
    expiresIn: Number(process.env.AWS_S3_EXPIRES_IN),
  },
});
