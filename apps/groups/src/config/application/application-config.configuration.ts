import { ApplicationConfig } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfig => ({
  application: {
    port: Number(process.env.GROUPS_PORT),
    host: process.env.GROUPS_HOST,
  },
});
