export interface ApplicationConfig {
  application: {
    port: number;
    host: string;
  };
}
