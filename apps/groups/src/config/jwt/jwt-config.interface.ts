import { JwtModuleOptions } from '@nestjs/jwt';

export interface JwtConfig {
  jwt: JwtModuleOptions & {
    accessTokenSecret: string;
    accessTokenExpiresIn: number;
    refreshTokenSecret: string;
    refreshTokenExpiresIn: number;
  };
}
