import { Injectable } from '@nestjs/common';

import { ConfigService } from '@nestjs/config';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';
import { JwtConfig } from './jwt-config.interface';

@Injectable()
export class JwtConfigFactory implements JwtOptionsFactory {
  constructor(private readonly configService: ConfigService<JwtConfig>) {}
  createJwtOptions(): JwtModuleOptions {
    const jwtOptions = this.configService.get('jwt');

    return jwtOptions;
  }
}
