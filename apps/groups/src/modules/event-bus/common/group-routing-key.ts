export enum GroupRoutingKey {
  studentCreatedEvent = 'student.outbound.event.studentCreated',
  teacherLeftClass = 'group.outbound.event.teacherLeftClass',
}
//   studentCreatedEvent = 'student.outbound.event.studentCreated',
//   getGroupByEmail = 'student.getstudentByEmail',
//   getGroupsByIds = 'student.inbound.rpc.getstudentsByIds',
//   getGroupById = 'student.inbound.rpc.getstudentById',
//   getstudentsByEmails = 'student.getstudentsByEmails',
//   createActivestudent = 'student.mutation.createActivestudent',
//   createInactivestudent = 'student.mutation.createInactivestudent',
//   updatestudentAuthProvider = 'student.mutation.updateAuthProvider',
//   setstudentPassword = 'student.inbound.rpc.setstudentPassword',
//   updatestudentInfo = 'student.inbound.rpc.updatestudentInfo',
//   updatestudentEmail = 'student.inbound.rpc.updatestudentEmail',
//
