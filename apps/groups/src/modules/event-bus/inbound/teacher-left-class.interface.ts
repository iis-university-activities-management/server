import { ConsumeMessage } from 'amqplib';
import { GroupRoutingKey } from '../common';

export enum Exchange {
  direct = 'amq.direct',
  topic = 'amq.topic',
  fanout = 'amq.fanout',
}

export abstract class TeacherLeftClassAgentInterface {
  static config = {
    exchange: Exchange.direct,
    routingKey: GroupRoutingKey.teacherLeftClass,
  };
  abstract execute(input: any, msg?: ConsumeMessage);
}
