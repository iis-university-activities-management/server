import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { IStudent } from '../../../../../students/src/modules/student/domain';
import {
  ClassRepository,
  ClassSubgroupRelationRepository,
} from '../../../../../subjects/src/modules/subject/infrastructure';
import { SubgroupHasStudentRelationRepository } from '../../group/infrastructure';
import { GroupQueue } from '../common';
import { TeacherLeftClassAgentInterface } from './teacher-left-class.interface';

interface TeacherLeftClassAgentInput {
  teacher: {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
  };
  classId: number;
  message: string;
}

const weeks = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

@Injectable()
export class TeacherLeftClassAgent implements TeacherLeftClassAgentInterface {
  private readonly logger = new Logger(TeacherLeftClassAgent.name);

  constructor(
    private readonly subgroupHasStudentRelationRepository: SubgroupHasStudentRelationRepository,
    private readonly classSubgroupRelationRepository: ClassSubgroupRelationRepository,
    private readonly classRepository: ClassRepository,
    private readonly mailerService: MailerService,
  ) {}

  @RabbitSubscribe({
    ...TeacherLeftClassAgentInterface.config,
    queue: GroupQueue.groupQueue,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: TeacherLeftClassAgentInput) {
    this.logger.log('[AGENT STARTING]');

    const { classId, message, teacher } = params;

    const leftClass = await this.classRepository.getById(classId);

    if (leftClass) {
      console.log({ leftClass });

      const subgroups =
        await this.classSubgroupRelationRepository.getRightsLabel(leftClass.id);

      console.log({ subgroups });

      const students: Array<IStudent> = [];

      for (const subgroup of subgroups) {
        const studs =
          await this.subgroupHasStudentRelationRepository.getRightsLabel(
            subgroup.id,
          );
        students.push(...studs);
      }

      console.log({ students });

      for (const student of students) {
        if (student.email) {
          this.logger.log(`Sent message to email: ${student.email}`);

          await this.mailerService.sendMail({
            to: student.email,
            // from: '"Support Team" <support@example.com>', // override default from
            subject: 'IIS Notification',
            template: './teacher-left-event', // `.hbs` extension is appended automatically
            context: {
              // ✏️ filling curly brackets with content
              teacher: `${teacher.lastName} ${teacher.firstName} ${teacher.middleName}`,
              message:
                message ||
                ` Teacher is late for a class <${leftClass.name}> ${
                  weeks[Number(leftClass.dayOfTheWeek)]
                } at ${leftClass.starts}`,
            },
          });
        }
      }
    }

    this.logger.log('[AGENT FINISHED]');
  }
}
