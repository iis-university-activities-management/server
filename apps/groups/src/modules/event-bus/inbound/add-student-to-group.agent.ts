import {
  MessageHandlerErrorBehavior,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import {
  GroupRepository,
  GroupHasStudentRelationRepository,
  GroupSubgroupRelationRepository,
  SubgroupHasStudentRelationRepository,
} from '../../group/infrastructure';
import { GroupQueue } from '../common';
import { AddStudentToGroupAgentInterface } from './add-student-to-group.interface';

interface CreateStudentAgentInput {
  student: {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    birthDay: string;
  };
  groupName: string;
}

@Injectable()
export class AddStudentToGroupAgent implements AddStudentToGroupAgentInterface {
  private readonly logger = new Logger(AddStudentToGroupAgent.name);

  constructor(
    private readonly groupRepository: GroupRepository,
    private readonly groupHasStudentRelationRepository: GroupHasStudentRelationRepository,
    private readonly subgroupHasStudentRelationRepository: SubgroupHasStudentRelationRepository,
    private readonly groupSubgroupRelationRepository: GroupSubgroupRelationRepository,
  ) {}

  @RabbitSubscribe({
    ...AddStudentToGroupAgentInterface.config,
    queue: GroupQueue.studentQueue,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async execute(params: CreateStudentAgentInput) {
    this.logger.log('[AGENT STARTING]');
    this.logger.log(params);

    const { groupName, student } = params;

    const group = await this.groupRepository.getGroupByName(groupName);

    if (group) {
      this.logger.log({ group });

      if (!group) {
        return;
      }

      if (group) {
        const relation = await this.groupHasStudentRelationRepository.saveByIds(
          group.id,
          {},
          student.id,
        );

        this.logger.log(relation);
      }

      const subgroups =
        await this.groupSubgroupRelationRepository.getRightsLabel(group.id);

      if (subgroups && subgroups.length > 0) {
        const subgroupsStudentsLenght: Array<number> = [];

        for (const subgroup of subgroups) {
          const students =
            await this.subgroupHasStudentRelationRepository.getRightsLabel(
              subgroup.id,
            );
          subgroupsStudentsLenght.push(students.length);
        }
        const minIndex = subgroupsStudentsLenght.indexOf(
          Math.min(...subgroupsStudentsLenght),
        );
        const subgroupWithMinStudents = subgroups[minIndex];

        const relation =
          await this.subgroupHasStudentRelationRepository.saveByIds(
            subgroupWithMinStudents.id,
            {},
            student.id,
          );

        this.logger.log(relation);
      }
    }

    this.logger.log('[AGENT FINISHED]');
  }
}
