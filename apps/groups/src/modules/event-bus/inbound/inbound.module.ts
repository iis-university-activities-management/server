import { Module } from '@nestjs/common';
import { OutboundModule } from '../outbound';
import { Neo4jModule } from '@uam/neo4j';
import { Neo4jConfigFactory } from '../../../config';
import { AddStudentToGroupAgent } from './add-student-to-group.agent';

import {
  GroupRepository,
  GroupHasStudentRelationRepository,
  GroupSubgroupRelationRepository,
  SubgroupHasStudentRelationRepository,
  ScheduleClassRelationRepository,
  GroupScheduleRelationRepository,
} from '../../group/infrastructure';
import { TeacherLeftClassAgent } from './teacher-left-class.agent';
import { MailerModule } from '@nestjs-modules/mailer';
import { resolve } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import {
  ClassRepository,
  ClassSubgroupRelationRepository,
} from '../../../../../subjects/src/modules/subject/infrastructure';

console.log(resolve(__dirname, '..', '..', '..', 'templates'));

@Module({
  imports: [
    OutboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
          user: 'igrakkaunt@gmail.com',
          pass: 'nbcroowkjsdyewxd',
        },
      },
      defaults: {
        from: '"IIS 2.0" <iis@uam.com>',
      },
      template: {
        dir: resolve(__dirname, '..', '..', '..', 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [
    AddStudentToGroupAgent,
    TeacherLeftClassAgent,
    GroupRepository,
    GroupHasStudentRelationRepository,
    GroupSubgroupRelationRepository,
    ScheduleClassRelationRepository,
    GroupScheduleRelationRepository,
    SubgroupHasStudentRelationRepository,

    ClassSubgroupRelationRepository,
    ClassRepository,
  ],
})
export class InboundModule {}
