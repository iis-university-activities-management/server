import { Module } from '@nestjs/common';
import { GroupCreatedEvent } from './events/group-created.event';

@Module({
  providers: [GroupCreatedEvent],
  exports: [GroupCreatedEvent],
})
export class OutboundModule {}
