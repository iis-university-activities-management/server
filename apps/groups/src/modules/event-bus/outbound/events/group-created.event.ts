import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { GroupCreatedEventInterface } from './group-created-event.interface';

@Injectable()
export class GroupCreatedEvent implements GroupCreatedEventInterface {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  execute(input: any) {
    const { exchange, routingKey } = GroupCreatedEventInterface.config;
    this.amqpConnection.publish(exchange, routingKey, input);
  }
}
