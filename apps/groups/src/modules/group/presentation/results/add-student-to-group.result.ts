import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { GroupDto } from '../dtos';

export class AddStudentToGroupResult {
  @ApiProperty({ type: Object, nullable: true })
  data?: any;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
