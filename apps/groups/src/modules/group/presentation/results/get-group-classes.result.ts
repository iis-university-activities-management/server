import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { GroupClassDto } from '../dtos';

export class GetGroupScheduleResult {
  @ApiProperty({ type: GroupClassDto, isArray: true, nullable: true })
  data?: Array<GroupClassDto>;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
