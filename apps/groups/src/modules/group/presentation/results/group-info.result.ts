import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { GroupDto } from '../dtos';

export class GroupInfoResult {
  @ApiProperty({ type: GroupDto, nullable: true })
  data?: GroupDto;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
