import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { GroupDto } from '../dtos';

export class AddStudentToSubgroupResult {
  @ApiProperty({ type: Object, nullable: true })
  data?: any;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
