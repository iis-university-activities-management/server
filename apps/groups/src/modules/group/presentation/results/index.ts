export * from './group-info.result';
export * from './add-student-to-group.result';
export * from './add-student-to-subgroup.result';
export * from './create-subgroup.result';
export * from './get-group-classes.result';
