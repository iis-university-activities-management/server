import { ApiProperty } from '@nestjs/swagger';
import { BaseException } from '@uam/exceptions';
import { GroupDto, SubgroupDto } from '../dtos';

export class CreateSubgroupResult {
  @ApiProperty({ type: SubgroupDto, nullable: true })
  data?: SubgroupDto;

  @ApiProperty({ type: BaseException, nullable: true })
  error?: BaseException;
}
