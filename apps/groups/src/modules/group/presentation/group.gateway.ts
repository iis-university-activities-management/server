import { WebSocketGateway } from '@nestjs/websockets';
import { GroupService } from '../application';

@WebSocketGateway({ namespace: 'groups' })
export class GroupWsGateway {
  constructor(private readonly studentService: GroupService) {}
}
