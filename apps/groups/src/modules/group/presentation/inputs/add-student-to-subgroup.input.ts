import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class AddStudentToSubgroupInput {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  subgroupId: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  studentId: number;
}
