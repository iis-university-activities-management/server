import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class AddStudentToGroupInput {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  groupId: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  studentId: number;
}
