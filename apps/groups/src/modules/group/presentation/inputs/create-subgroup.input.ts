import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateSubgroupInput {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  groupId: number;
}
