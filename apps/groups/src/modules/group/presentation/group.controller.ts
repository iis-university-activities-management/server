import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { GroupService } from '../application';
import { CreateGroupInput, CreateSubgroupInput } from './inputs';
import {
  CreateSubgroupResult,
  GetGroupScheduleResult,
  GroupInfoResult,
} from './results';

@ApiTags('Groups Info')
@Controller('v1/groups')
@UsePipes(ValidationPipe)
export class GroupContoller {
  constructor(private readonly groupService: GroupService) {}

  @Get('/:id/info')
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiResponse({
    type: GroupInfoResult,
    description:
      'This endpoint returns complete group information by group ID. If the group was not found, it will return a 404 error',
  })
  async groupInfo(@Param('id') groupId: string): Promise<GroupInfoResult> {
    const data = await this.groupService.getGroupById({
      groupId: +groupId,
    });

    return {
      data,
    } as any;
  }

  @Post()
  @ApiBody({
    type: CreateGroupInput,
    required: true,
  })
  @ApiBearerAuth()
  async createGroup(@Body() input: CreateGroupInput): Promise<GroupInfoResult> {
    const { data } = await this.groupService.createGroup(input);

    return {
      data,
    };
  }

  @Put('/add/subgroup')
  @ApiBody({
    type: CreateSubgroupInput,
    required: true,
  })
  @ApiBearerAuth()
  async addSubgroup(
    @Body() input: CreateSubgroupInput,
  ): Promise<CreateSubgroupResult> {
    const { data } = await this.groupService.createSubgroup(input);

    return {
      data,
    };
  }

  @Get('/:groupId/schedule')
  @ApiBearerAuth()
  @ApiResponse({
    type: GetGroupScheduleResult,
  })
  async getGroupSchedule(
    @Param('groupId') groupId: string,
  ): Promise<GetGroupScheduleResult> {
    const { data } = await this.groupService.getGroupSchedule({
      groupId: +groupId,
    });

    return {
      data,
    };
  }
}
