import { ApiProperty } from '@nestjs/swagger';

export class SubgroupDto {
  @ApiProperty({ type: Number, nullable: false })
  id: number;

  @ApiProperty({ type: Number, nullable: false })
  subgroup: number;
}
