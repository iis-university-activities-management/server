import { ApiProperty } from '@nestjs/swagger';
import { GroupDto, SubgroupDto } from '.';
import { ClassDto } from '../../../../../../subjects/src/modules/subject/presentation/dtos';
import { TeacherDto } from '../../../../../../teachers/src/modules/teacher/presentation/dtos';

export class GroupClassDto {
  @ApiProperty({ type: ClassDto, nullable: false })
  class: ClassDto;

  @ApiProperty({ type: GroupDto, nullable: false })
  group: GroupDto;

  @ApiProperty({ type: TeacherDto, nullable: false })
  teacher: TeacherDto;

  @ApiProperty({ type: SubgroupDto, isArray: true, nullable: false })
  subgroups: Array<SubgroupDto>;
}
