import { ApiProperty } from '@nestjs/swagger';

export class GroupDto {
  @ApiProperty({ type: Number, nullable: false })
  id: number;

  @ApiProperty({ type: String, nullable: false })
  name: string;

  @ApiProperty({ type: Number, nullable: false })
  course: number;
}
