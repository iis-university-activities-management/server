import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

import { IGroupSubgroupRelation } from '../../domain';

@Relation()
export class GroupSubgroupRelation implements IGroupSubgroupRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
