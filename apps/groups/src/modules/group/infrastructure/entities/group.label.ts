import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { IGroup } from '../../domain';

@Label()
export class Group implements IGroup {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  name: string;

  @Property()
  course: number;
}
