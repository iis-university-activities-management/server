import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

import { IScheduleClassRelation } from '../../domain';

@Relation()
export class ScheduleClassRelation implements IScheduleClassRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
