import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

import { ISubgroupHasStudentRelation } from '../../domain';

@Relation()
export class SubgroupHasStudentRelation implements ISubgroupHasStudentRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
