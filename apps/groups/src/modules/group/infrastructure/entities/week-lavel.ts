import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

@Label()
export class Week {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  number: number;
}
