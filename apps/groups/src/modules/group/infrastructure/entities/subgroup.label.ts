import { Label, PrimaryGeneratedColumn, Property } from '@uam/neo4j';

import { ISubgroup } from '../../domain';

@Label()
export class Subgroup implements ISubgroup {
  @PrimaryGeneratedColumn()
  id: number;

  @Property()
  subgroup: number;
}
