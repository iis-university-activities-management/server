import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

import { IGroupScheduleRelation } from '../../domain';

@Relation()
export class GroupScheduleRelation implements IGroupScheduleRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
