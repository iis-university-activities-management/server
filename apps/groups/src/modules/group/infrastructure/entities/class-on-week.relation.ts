import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

@Relation()
export class ClassOnWeekRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
