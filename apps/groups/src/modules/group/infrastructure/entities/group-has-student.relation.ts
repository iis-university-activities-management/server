import { PrimaryGeneratedColumn, Relation } from '@uam/neo4j';

import { IGroupHasStudentRelation } from '../../domain';

@Relation()
export class GroupHasStudentRelation implements IGroupHasStudentRelation {
  @PrimaryGeneratedColumn()
  id: number;
}
