import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jLabelRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Subgroup } from '../entities';

@Injectable()
export class SubgroupRepository extends Neo4jLabelRepository<Subgroup> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(Subgroup, neo4jService);
  }
}
