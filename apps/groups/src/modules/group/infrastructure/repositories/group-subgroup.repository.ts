import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Group, Subgroup, GroupSubgroupRelation } from '../entities';

@Injectable()
export class GroupSubgroupRelationRepository extends Neo4jRelationRepository<
  GroupSubgroupRelation,
  Group,
  Subgroup
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(GroupSubgroupRelation, Group, Subgroup, neo4jService);
  }
}
