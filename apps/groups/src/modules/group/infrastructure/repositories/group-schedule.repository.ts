import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Schedule } from '@uam/global';
import { Group, GroupScheduleRelation } from '../entities';

@Injectable()
export class GroupScheduleRelationRepository extends Neo4jRelationRepository<
  GroupScheduleRelation,
  Group,
  Schedule
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(GroupScheduleRelation, Group, Schedule, neo4jService);
  }
}
