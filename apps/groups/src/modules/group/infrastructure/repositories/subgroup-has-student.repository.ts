import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Student } from '../../../../../../students/src/modules/student/infrastructure';
import { Subgroup, SubgroupHasStudentRelation } from '../entities';

@Injectable()
export class SubgroupHasStudentRelationRepository extends Neo4jRelationRepository<
  SubgroupHasStudentRelation,
  Subgroup,
  Student
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(SubgroupHasStudentRelation, Subgroup, Student, neo4jService);
  }
}
