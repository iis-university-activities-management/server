import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { ClassOnWeekRelation } from '../entities';
import { Class } from '../../../../../../subjects/src/modules/subject/infrastructure';
import { Week } from '../entities';

@Injectable()
export class ClassOnWeekRelationRepository extends Neo4jRelationRepository<
  ClassOnWeekRelation,
  Class,
  Week
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(ClassOnWeekRelation, Class, Week, neo4jService);
  }
}
