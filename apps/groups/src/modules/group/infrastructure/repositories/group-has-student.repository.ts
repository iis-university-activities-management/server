import { Inject, Injectable } from '@nestjs/common';

import {
  Neo4jRelationRepository,
  NEO4J_CONNECTION,
  Neo4jConnection,
} from '@uam/neo4j';
import { Student } from '../../../../../../students/src/modules/student/infrastructure';
import { Group, GroupHasStudentRelation } from '../entities';

@Injectable()
export class GroupHasStudentRelationRepository extends Neo4jRelationRepository<
  GroupHasStudentRelation,
  Group,
  Student
> {
  constructor(
    @Inject(NEO4J_CONNECTION)
    neo4jService: Neo4jConnection,
  ) {
    super(GroupHasStudentRelation, Group, Student, neo4jService);
  }
}
