import { IStudent } from '../../../../../students/src/modules/student/domain';
import {
  IGroup,
  IGroupHasStudentRelation,
  ISubgroup,
  ISubgroupHasStudentRelation,
} from '../domain';

export interface GetGroupByIdParameters {
  groupId: number;
}
export interface CreateGroupParameters {
  name: string;
  course: number;
}
export interface CreateGroupResult {
  data: IGroup;
}

export interface AddSubgroupParameters {
  groupId: number;
}

export interface AddSubgroupResult {
  data: ISubgroup;
}

export interface AddStudentToSubgroupParameters {
  subgroupId: number;
  studentId: number;
}

export interface AddStudentToGroupParameters {
  groupId: number;
  studentId: number;
}

export interface AddStudentToSubgroupResult {
  data: {
    subgroup: ISubgroup;
    relation: ISubgroupHasStudentRelation;
    student: IStudent;
  };
}

export interface AddStudentToGroupResult {
  data: {
    group: IGroup;
    relation: IGroupHasStudentRelation;
    student: IStudent;
  };
}
