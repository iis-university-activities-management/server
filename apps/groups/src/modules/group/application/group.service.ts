import { Injectable } from '@nestjs/common';
import { BadRequestException } from '@uam/exceptions';
import { IClass } from '../../../../../subjects/src/modules/subject/domain';
import { ClassSubgroupRelationRepository } from '../../../../../subjects/src/modules/subject/infrastructure';
import { ITeacher } from '../../../../../teachers/src/modules/teacher/domain';
import { ClassTeacherRelationRepository } from '../../../../../teachers/src/modules/teacher/infrastructure';
import { IGroup, ISubgroup } from '../domain';

import {
  Group,
  GroupRepository,
  SubgroupRepository,
  GroupScheduleRelationRepository,
  GroupSubgroupRelationRepository,
  GroupHasStudentRelationRepository,
  SubgroupHasStudentRelationRepository,
  ScheduleClassRelationRepository,
  Week,
} from '../infrastructure';
import { ClassOnWeekRelationRepository } from '../infrastructure/repositories/class-on-week-ralation.repository';
import {
  AddStudentToGroupParameters,
  AddSubgroupParameters,
  AddSubgroupResult,
  CreateGroupParameters,
  CreateGroupResult,
  GetGroupByIdParameters,
} from './group-service.type';

@Injectable()
export class GroupService {
  constructor(
    private readonly groupRepository: GroupRepository,
    private readonly subgroupRepository: SubgroupRepository,
    private readonly groupScheduleRelationRepository: GroupScheduleRelationRepository,
    private readonly groupSubgroupRelationRepository: GroupSubgroupRelationRepository,
    private readonly groupHasStudentRelationRepository: GroupHasStudentRelationRepository,
    private readonly subgroupHasStudentRelationRepository: SubgroupHasStudentRelationRepository,
    private readonly scheduleClassRelationRepository: ScheduleClassRelationRepository,
    private readonly classTeacherRelationRepository: ClassTeacherRelationRepository,
    private readonly classSubgroupRelationRepository: ClassSubgroupRelationRepository,
    private readonly classOnWeekRelationRepository: ClassOnWeekRelationRepository,
  ) {}

  async getGroupById(params: GetGroupByIdParameters): Promise<Group> {
    const { groupId } = params;

    const group = await this.groupRepository.getById(groupId);

    if (!group) {
      throw new BadRequestException(
        'getGroupById',
        `group by id <${groupId}> not found`,
      );
    }

    return group;
  }

  async create() {
    // const speciality = await this.specialityRepository.save({
    //   name: 'Промышленная электроника',
    // });
    // const speciality2 = await this.specialityRepository.save({
    //   name: 'Автоматизированные системы обработки информации',
    // });
    // const speciality3 = await this.specialityRepository.save({
    //   name: 'Информационные технологии и управление в технических системах',
    // });
    // const relation = await this.groupSpecialtyRepository.saveByIds(
    //   0,
    //   { admissionAt: new Date().toISOString(), course: 3 },
    //   speciality.id,
    // );
    // console.log(speciality, speciality2, speciality3);
  }

  async createGroup(params: CreateGroupParameters): Promise<CreateGroupResult> {
    const { name, course } = params;

    const group = await this.groupRepository.save({ course, name });

    return { data: group };
  }

  async createSubgroup(
    params: AddSubgroupParameters,
  ): Promise<AddSubgroupResult> {
    const { groupId } = params;

    const subgroups = await this.groupSubgroupRelationRepository.getRightsLabel(
      groupId,
    );

    const newSubgroup = await this.subgroupRepository.save({
      subgroup: subgroups.length + 1,
    });

    await this.groupSubgroupRelationRepository.saveByIds(
      groupId,
      {},
      newSubgroup.id,
    );

    return { data: newSubgroup };
  }

  async getGroupSchedule(params: { groupId: number }) {
    const { groupId } = params;

    const schedule = await this.groupScheduleRelationRepository.getRightLabel(
      groupId,
    );

    const group = await this.groupRepository.getById(groupId);

    const classes = await this.scheduleClassRelationRepository.getRightsLabel(
      schedule.id,
    );

    const results: Array<{
      class: IClass;
      group: IGroup;
      subgroups: Array<ISubgroup>;
      teacher: ITeacher;
      weeks: Array<Week>;
    }> = [];

    for (const _class of classes) {
      const teacher = await this.classTeacherRelationRepository.getRightLabel(
        _class.id,
      );

      const weeks = await this.classOnWeekRelationRepository.getRightsLabel(
        _class.id,
      );

      const subgroups =
        await this.classSubgroupRelationRepository.getRightsLabel(_class.id);

      results.push({
        class: _class,
        group,
        subgroups,
        teacher,
        weeks,
      });
    }

    return {
      data: results,
    };
  }
}
