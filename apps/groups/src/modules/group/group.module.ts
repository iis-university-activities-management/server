import { Module } from '@nestjs/common';

import { Neo4jModule } from '@uam/neo4j';
import { ScheduleRepository, SpecialityRepository } from '@uam/global';

import { Neo4jConfigFactory } from '../../config';
import { InboundModule, OutboundModule } from '../event-bus';
import { GroupService } from './application';
import { GroupDomain } from './domain';
import {
  GroupRepository,
  SubgroupRepository,
  GroupScheduleRelationRepository,
  GroupSubgroupRelationRepository,
  GroupHasStudentRelationRepository,
  SubgroupHasStudentRelationRepository,
  ScheduleClassRelationRepository,
} from './infrastructure';
import {
  GroupContoller,
  GroupWsGateway,
  GroupResolver,
  GroupHandler,
} from './presentation';
import { ClassTeacherRelationRepository } from '../../../../teachers/src/modules/teacher/infrastructure';
import { ClassSubgroupRelationRepository } from '../../../../subjects/src/modules/subject/infrastructure';
import { ClassOnWeekRelationRepository } from './infrastructure/repositories/class-on-week-ralation.repository';

@Module({
  imports: [
    OutboundModule,
    InboundModule,
    Neo4jModule.forRootAsync({
      useClass: Neo4jConfigFactory,
    }),
  ],
  providers: [
    GroupDomain,
    GroupService,
    GroupWsGateway,
    GroupHandler,
    GroupResolver,
    GroupRepository,
    SpecialityRepository,
    SubgroupRepository,
    ScheduleRepository,
    ScheduleClassRelationRepository,
    GroupScheduleRelationRepository,
    ClassTeacherRelationRepository,
    ClassSubgroupRelationRepository,
    ClassOnWeekRelationRepository,
    GroupSubgroupRelationRepository,
    GroupHasStudentRelationRepository,
    SubgroupHasStudentRelationRepository,
  ],
  controllers: [GroupContoller],
})
export class GroupModule {}
