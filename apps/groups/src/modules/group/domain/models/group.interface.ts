export interface IGroup {
  id: number;
  name: string;
  course: number;
}
