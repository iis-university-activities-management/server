export * from './group.interface';
export * from './subgroup.interface';
export * from './group-schedule-relation.interface';
export * from './group-subgroup-relation.interface';
export * from './group-has-student-relation.interface';
export * from './subgroup-has-student-relation.interface';
export * from './class-schedule-relation.interface';
